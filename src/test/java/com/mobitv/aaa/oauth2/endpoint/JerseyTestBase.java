package com.mobitv.aaa.oauth2.endpoint;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerException;
import com.sun.jersey.test.framework.spi.container.TestContainerFactory;
import com.sun.jersey.test.framework.spi.container.grizzly.web.GrizzlyWebTestContainerFactory;

public class JerseyTestBase extends JerseyTest {

	protected final String BASE_URI_IDENTITY_SERVICE		= "/identity/v5/oauth2";
	protected final String BASE_URI_SESSION_SERVICE			= "/core/v5/session";
	protected final String BASE_URI_PLATFORM_SERVICE		= "/platform/v5/oauth2";
	protected final String CARRIER 							= "mobitv";
	protected final String PRODUCT 							= "reference";
	protected final String VERSION 							= "5.0";
	
	protected final String DEFAULT_SCOPE 					= "role_verified_identity";

	protected final String GRANT_TYPE_GATEWAY_HEADER		= GrantType.gateway_header.toString();
	protected final String GRANT_TYPE_REFRESH_TOKEN		= GrantType.refresh_token.toString();
	protected final String GRANT_TYPE_PASSWORD				= GrantType.password.toString();
	protected final String GRANT_TYPE_PIN					= GrantType.pin.toString();
	
	protected final String MOCK_PARTNER_PROFILE_ID			= "mockPartnerProfileId";
	protected final String MOCK_EMAIL 						= "mockEmail";
	protected final String MOCK_ACCOUNT_ID					= "mockAccountId";
	protected final String MOCK_PROFILE_ID					= "mockProfileId";
	
	@Override
	protected TestContainerFactory getTestContainerFactory()
			throws TestContainerException {
		return new GrizzlyWebTestContainerFactory();
	}
	
	public JerseyTestBase() {
		 
        super(new WebAppDescriptor.Builder("com.mobitv.aaa.oauth2.endpoint") 
        .contextPath("mobi-aaa-stub-identity-manager-oauth2") 
        .contextParam("contextConfigLocation", "classpath:application-context-unittest.xml") 		// load the default applicationContext
        .contextListenerClass(ContextLoaderListener.class)
        .requestListenerClass(RequestContextListener.class)
        .servletPath("/*")
        .servletClass(SpringServlet.class)
        .initParam("JSONConfiguration.Builder.rootUnwrapping", "true")
        .initParam("com.sun.jersey.api.json.POJOMappingFeature", "false")
        .initParam("com.sun.jersey.config.property.MediaTypeMappings", "json : application/json, xml : application/xml, lua : application/x-lua, lua-txt : text/x-lua")
        .initParam("com.sun.jersey.spi.container.ContainerRequestFilters", "com.mobitv.commons.logging.filter.JerseyLoggingFilter, com.sun.jersey.api.container.filter.PostReplaceFilter")
        .initParam("com.sun.jersey.spi.container.ContainerResponseFilters", "com.sun.jersey.api.container.filter.LoggingFilter")
        .build());
    }
	
	@SuppressWarnings("unchecked")
	protected <T> T getBean(String beanId, Class<T> clazz) {
		Object bean = ApplicationContextProvider.getApplicationContext()
				.getBean(beanId);
		return (T) bean;
	}
	
}
