/**
 * 
 */
package com.mobitv.aaa.oauth2.endpoint;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.MediaType;

import junit.framework.Assert;

import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.authnz.dto.TokenInfoRequest;
import com.mobitv.aaa.service.OAuth2TokenService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.impl.AccountManagerHelper;
import com.mobitv.exceptionwrapper.dto.ErrorWrapper;
import com.mobitv.platform.core.dto.Device;
import com.mobitv.platform.core.dto.Profile;
import com.mobitv.platform.identity.dto.AuthorizationCodeRequest;
import com.mobitv.platform.identity.dto.AuthorizationCodeResponse;
import com.mobitv.platform.identity.dto.Token;
import com.mobitv.platform.identity.dto.TokensRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

public class IdentityServiceTestCase extends JerseyTestBase {
	
	private IdentityServiceImpl identityServiceImpl; 
	private AccountManagerHelper mockAccountManagerHelper;
	private Profile MOCK_PROFILE; 
	
	public IdentityServiceTestCase() {
		super();
	}
	
	@Before
	public void init() {

		MOCK_PROFILE = new Profile();
		MOCK_PROFILE.setAccountId(MOCK_ACCOUNT_ID);
		MOCK_PROFILE.setProfileId(MOCK_PROFILE_ID);
		MOCK_PROFILE.setEmail(MOCK_EMAIL);
		MOCK_PROFILE.setPartnerProfileId(MOCK_PARTNER_PROFILE_ID);
		
		identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		Assert.assertNotNull(identityServiceImpl);
		
		mockAccountManagerHelper = Mockito.mock(AccountManagerHelper.class);
		identityServiceImpl.setAccountManagerHelper(mockAccountManagerHelper);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetAccessTokenWithRefreshToken() throws Exception {
		String vuid	= UUID.randomUUID().toString();
		String clientId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String profileId = UUID.randomUUID().toString();
		String partnerProfileId = Base64.encodeBase64URLSafeString(MessageDigest.getInstance("MD5").digest(vuid.getBytes()));
		String email = partnerProfileId + "@mobitv.com";
		
		final IdentityServiceImpl identityService = getBean("identityService", IdentityServiceImpl.class);
		final SecureTokenService secureTokenService = getBean("simpleTokenService", SecureTokenService.class);
		
		Profile profile = new Profile();
		profile.setAccountId(accountId);
		profile.setProfileId(profileId);
		mockAccountManagementRegisterUser(identityService, partnerProfileId, email, profile);
		
		try {
			// Get access token and validate response
			ClientResponse response = getAccessTokensWithGatewayHeader(vuid,
					clientId);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
			Token tokenResponseEntity = response.getEntity(Token.class);
			Assert.assertNotNull(tokenResponseEntity);
			Assert.assertNotNull(tokenResponseEntity.getAccessToken());
			Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

			@SuppressWarnings("rawtypes")
			TokenData accessTokenData = secureTokenService.unmarshall(tokenResponseEntity.getAccessToken());
			assertNotNull(accessTokenData);
			assertEquals(profileId, accessTokenData.getProfileID());
			assertEquals(accountId, accessTokenData.getAccountID());
			assertEquals(clientId, accessTokenData.getCid());
			
			List<Statement> statements = accessTokenData.getStatements();
			assertNotNull(statements);
			boolean vuidStatementPresent = false;
			
			for (Statement statement : statements) {
				if ("x-mobitv-vuid".equals(statement.getName())) {
					vuidStatementPresent = true;
					break;
				}
			}
			assertTrue(vuidStatementPresent);
			
			@SuppressWarnings("rawtypes")
			TokenData refreshTokenData = secureTokenService.unmarshall(tokenResponseEntity.getRefreshToken());
			assertNotNull(refreshTokenData);
			assertEquals(profileId, refreshTokenData.getProfileID());
			assertEquals(accountId, refreshTokenData.getAccountID());
			assertEquals(clientId, refreshTokenData.getCid());
			
			statements = refreshTokenData.getStatements();
			assertNotNull(statements);
			vuidStatementPresent = false;
			
			for (Statement statement : statements) {
				if ("x-mobitv-vuid".equals(statement.getName())) {
					vuidStatementPresent = true;
					break;
				}
			}
			assertTrue(vuidStatementPresent);
			
			response = getAccessTokensWithRefreshToken(tokenResponseEntity.getRefreshToken());
			assertNotNull(response);
			assertEquals(Status.OK, response.getClientResponseStatus());
			
			tokenResponseEntity = response.getEntity(Token.class);
			Assert.assertNotNull(tokenResponseEntity);
			Assert.assertNotNull(tokenResponseEntity.getAccessToken());
			Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

			accessTokenData = secureTokenService.unmarshall(tokenResponseEntity.getAccessToken());
			assertNotNull(accessTokenData);
			assertEquals(profileId, accessTokenData.getProfileID());
			assertEquals(accountId, accessTokenData.getAccountID());
			assertEquals(clientId, accessTokenData.getCid());
			
			statements = accessTokenData.getStatements();
			assertNotNull(statements);
			vuidStatementPresent = false;
			
			for (Statement statement : statements) {
				if ("x-mobitv-vuid".equals(statement.getName())) {
					vuidStatementPresent = true;
					break;
				}
			}
			assertTrue(vuidStatementPresent);
			
			refreshTokenData = secureTokenService.unmarshall(tokenResponseEntity.getRefreshToken());
			assertNotNull(refreshTokenData);
			assertEquals(profileId, refreshTokenData.getProfileID());
			assertEquals(accountId, refreshTokenData.getAccountID());
			assertEquals(clientId, refreshTokenData.getCid());
			
			statements = refreshTokenData.getStatements();
			assertNotNull(statements);
			vuidStatementPresent = false;
			
			for (Statement statement : statements) {
				if ("x-mobitv-vuid".equals(statement.getName())) {
					vuidStatementPresent = true;
					break;
				}
			}
			assertTrue(vuidStatementPresent);
		} catch (UniformInterfaceException e) {
			System.out.println("HTTP_STATUS = " + e.getResponse().getStatus());
			System.out.println("error_response = " + e.getResponse().getEntity(String.class));
			Assert.fail("UniformInterfaceException");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception");
		}
	}
	
	private ClientResponse getAccessTokensWithRefreshToken(String refreshToken) {
		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/tokens.json");
		Builder builder = wr.header("Content-Type", "application/json");

		TokensRequest requestEntity = new TokensRequest();
		requestEntity.setGrantType(GRANT_TYPE_REFRESH_TOKEN);
		requestEntity.setRefreshToken(refreshToken);

		ClientResponse response = builder.post(ClientResponse.class,
				requestEntity);
		return response;
	}

	@Test
	public void test_tokens_GATEWAYHEADER() throws NoSuchAlgorithmException {
		String vuid	= UUID.randomUUID().toString();
		String clientId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String profileId = UUID.randomUUID().toString();
		String partnerProfileId = Base64.encodeBase64URLSafeString(MessageDigest.getInstance("MD5").digest(vuid.getBytes()));
		String email = partnerProfileId + "@mobitv.com";
		
		final IdentityServiceImpl identityService = getBean("identityService", IdentityServiceImpl.class);
		
		Profile profile = new Profile();
		profile.setAccountId(accountId);
		profile.setProfileId(profileId);
		mockAccountManagementRegisterUser(identityService, partnerProfileId, email, profile);
		
		try {
			ClientResponse response = getAccessTokensWithGatewayHeader(vuid,
					clientId);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
			Token tokenResponseEntity = response.getEntity(Token.class);
			Assert.assertNotNull(tokenResponseEntity);
			Assert.assertNotNull(tokenResponseEntity.getAccessToken());
			Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

			System.out.println(tokenResponseEntity.toString());
		} catch (UniformInterfaceException e) {
			System.out.println("HTTP_STATUS = " + e.getResponse().getStatus());
			System.out.println("error_response = " + e.getResponse().getEntity(String.class));
			Assert.fail("UniformInterfaceException");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception");
		}
	}

	private ClientResponse getAccessTokensWithGatewayHeader(String vuid,
			String clientId) {
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("x-mobitv-vuid", vuid);

		TokensRequest requestEntity = new TokensRequest();
		requestEntity.setGrantType(GRANT_TYPE_GATEWAY_HEADER);
		requestEntity.setScope(DEFAULT_SCOPE);
		requestEntity.setClientId(clientId);
		
		ClientResponse response = builder.post(ClientResponse.class, requestEntity);
		return response;
	}
	    
	@Test
	public void test_verify_tokens_GATEWAYHEADER_platform_endpoint() {
		
		long time = System.currentTimeMillis();
		
		String vuid	= "VUID_" + time;
		
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("x-mobitv-vuid", vuid);

		TokensRequest tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_GATEWAY_HEADER);
		tokenRequestEntity.setScope(DEFAULT_SCOPE);
		tokenRequestEntity.setClientId(String.valueOf(time));
		
		// Mock the call to register account 
		Mockito.when(mockAccountManagerHelper.registerUser(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);
		
		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
					.registerUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		Token tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());
		Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

		System.out.println(tokenResponseEntity.toString());
		
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		TokenInfoRequest tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
	}
	
	@Test
	public void test_verify_tokens_GATEWAYHEADER_identity_endpoint() {
		
		long time = System.currentTimeMillis();
		
		String vuid	= "VUID_" + time;
		
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("x-mobitv-vuid", vuid);

		TokensRequest tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_GATEWAY_HEADER);
		tokenRequestEntity.setScope(DEFAULT_SCOPE);
		tokenRequestEntity.setClientId(String.valueOf(time));
		
		// Mock the call to register account 
		Mockito.when(mockAccountManagerHelper.registerUser(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);
		
		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
					.registerUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		Token tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());
		Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

		System.out.println(tokenResponseEntity.toString());
		
		wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		TokenInfoRequest tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
	}
	
	@Test
	public void test_verify_tokens_PASSWORD_platform_endpoint() {
		
		long time = System.currentTimeMillis();
		
		String username = "test1";
		String password = "test1";
		
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("accept", MediaType.APPLICATION_JSON);

		TokensRequest tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_PASSWORD);
		tokenRequestEntity.setClientId(String.valueOf(time));
		tokenRequestEntity.setUsername(username);
		tokenRequestEntity.setPassword(password);
		
		// Mock the call to register account 
		Mockito.when(mockAccountManagerHelper.registerUser(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);
		
		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
					.registerUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		Token tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());
		//Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

		System.out.println(tokenResponseEntity.toString());
		
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		TokenInfoRequest tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
	}
	
	@Test
	public void test_tokens_PIN_gateway_header() {
		
		long time = System.currentTimeMillis();
		
		String vuid	= "VUID_" + time;
		String pin = "mockPin";
		
		// 1. request new tokens => grant_type=gateway_header
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("x-mobitv-vuid", vuid)
							.header("accept", MediaType.APPLICATION_JSON);

		TokensRequest tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_GATEWAY_HEADER);
		tokenRequestEntity.setScope(DEFAULT_SCOPE);
		tokenRequestEntity.setClientId(String.valueOf(time));
		
		// Mock the call to register account 
		Mockito.when(mockAccountManagerHelper.registerUser(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);

		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
				.registerUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		Token tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());

		System.out.println(tokenResponseEntity.toString());
		
		// 2. verify tokens
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		TokenInfoRequest tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		// 3. request tokens => grant_type=pin
		wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");

		builder = wr.header("accept", MediaType.APPLICATION_JSON);
		
		tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_PIN);
		tokenRequestEntity.setRefreshToken(tokenResponseEntity.getRefreshToken());
		tokenRequestEntity.setPin(pin);
		
		Mockito.when(mockAccountManagerHelper.validatePin(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		response = builder.post(ClientResponse.class, tokenRequestEntity);
		
		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
				.validatePin(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());
		
		// 4. verify tokens
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
	}
	
	@Test
	public void test_tokens_PIN_password() {
		
		long time = System.currentTimeMillis();
		
		String username = "test1";
		String password = "test1";
		String pin = "mockPin";
		
		// 1. request new tokens => grant_type=gateway_header
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
									+ "/" + CARRIER
									+ "/" + PRODUCT
									+ "/" + VERSION
									+ "/tokens.json");
		
		Builder builder = wr.header("accept", MediaType.APPLICATION_JSON);

		TokensRequest tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_PASSWORD);
		tokenRequestEntity.setClientId(String.valueOf(time));
		tokenRequestEntity.setUsername(username);
		tokenRequestEntity.setPassword(password);
		
		// Mock the call to register account 
		Mockito.when(mockAccountManagerHelper.registerUser(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);

		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
				.registerUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		Token tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());

		System.out.println(tokenResponseEntity.toString());
		
		// 2. verify tokens
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		TokenInfoRequest tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		// 3. request tokens => grant_type=pin
		wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");

		builder = wr.header("accept", MediaType.APPLICATION_JSON);
		
		tokenRequestEntity = new TokensRequest();
		tokenRequestEntity.setGrantType(GRANT_TYPE_PIN);
		tokenRequestEntity.setRefreshToken(tokenResponseEntity.getRefreshToken());
		tokenRequestEntity.setPin(pin);
		
		Mockito.when(mockAccountManagerHelper.validatePin(
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())
				).thenReturn(MOCK_PROFILE);
		
		response = builder.post(ClientResponse.class, tokenRequestEntity);
		
		Mockito.verify(mockAccountManagerHelper, Mockito.times(1))
				.validatePin(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		tokenResponseEntity = response.getEntity(Token.class);
		Assert.assertNotNull(tokenResponseEntity);
		Assert.assertNotNull(tokenResponseEntity.getAccessToken());
		
		// 4. verify tokens
		wr = resource().path(BASE_URI_PLATFORM_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens/verify.json");
		
		tokenInfoRequestEntity = new TokenInfoRequest();
		tokenInfoRequestEntity.setAccess_token(tokenResponseEntity.getAccessToken());
		
		response = wr.post(ClientResponse.class, tokenInfoRequestEntity);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testGetAuthorizationCode_GuestPlayBack_Success() throws Exception {
		final IdentityServiceImpl identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		final OAuth2TokenService oAuth2TokenService = getBean("oAuth2TokenService", OAuth2TokenService.class);
		final SecureTokenService tokenService = getBean("simpleTokenService", SecureTokenService.class);
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		final TokenData accessTokenData = new TokenData();
		final String accountId = "accountId";
		final String profileId = "profileId";
		final String deviceSerialId = "deviceSerialId";
		final Date currentDate = new Date();
		
		assertNotNull(identityServiceImpl);
		assertNotNull(oAuth2TokenService);
		
		accessTokenData.setAccountID(accountId);
		accessTokenData.setProfileID(profileId);
		List<String> scopes = new ArrayList<String>();
		scopes.add("role_verified_identity");
		accessTokenData.setScope(scopes);
		accessTokenData.setCreationDate(currentDate);
		accessTokenData.setExpires_in(3600 * 8);  // 8 hours
		
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK.getLabel());
		authorizationCodeRequest.setAccessToken(tokenService.marshall(accessTokenData));
		authorizationCodeRequest.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setClientId(deviceSerialId);
		authorizationCodeRequest.setResponseType("code");
		
		//
		// Create and setup mocks
		//
		// NO MOCKS REQUIRED
		
		//
		// Call grizzly localhost
		//
		final String vuid = "VUID_" + currentDate.getTime();
		final  WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/authorization_code.json");
		
		final Builder builder = wr.header("x-mobitv-vuid", vuid);
		final ClientResponse response = builder.post(ClientResponse.class, authorizationCodeRequest);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		final AuthorizationCodeResponse authorizationCodeResponse = response.getEntity(AuthorizationCodeResponse.class);
		Assert.assertNotNull(authorizationCodeResponse.getCode());
		final TokenData<String> responseCode = tokenService.unmarshall(authorizationCodeResponse.getCode());
		Assert.assertNotNull(responseCode);
		Assert.assertEquals(accountId, responseCode.getAccountID());
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testGetAuthorizationCode_RemoteControlPlayBack_Success() throws Exception {
		final IdentityServiceImpl identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		final OAuth2TokenService oAuth2TokenService = getBean("oAuth2TokenService", OAuth2TokenService.class);
		final SecureTokenService tokenService = getBean("simpleTokenService", SecureTokenService.class);
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		final TokenData accessTokenData = new TokenData();
		final String accountId = "accountId";
		final String profileId = "profileId";
		final String deviceSerialId = "deviceSerialId";
		final Date currentDate = new Date();
		
		assertNotNull(identityServiceImpl);
		assertNotNull(oAuth2TokenService);
		
		accessTokenData.setAccountID(accountId);
		accessTokenData.setProfileID(profileId);
		List<String> scopes = new ArrayList<String>();
		scopes.add("role_verified_identity");
		accessTokenData.setScope(scopes);
		accessTokenData.setCreationDate(currentDate);
		accessTokenData.setExpires_in(3600 * 8);  // 8 hours
		
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP.getLabel());
		authorizationCodeRequest.setAccessToken(tokenService.marshall(accessTokenData));
		authorizationCodeRequest.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setClientId(deviceSerialId);
		authorizationCodeRequest.setResponseType("code");
		
		mockAccountManagementGetDevice(identityServiceImpl, profileId, deviceSerialId);
		
		//
		// Call grizzly localhost
		//
		final String vuid = "VUID_" + currentDate.getTime();
		final  WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/authorization_code.json");
		
		final Builder builder = wr.header("x-mobitv-vuid", vuid);
		final ClientResponse response = builder.post(ClientResponse.class, authorizationCodeRequest);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.OK, response.getClientResponseStatus());
		
		final AuthorizationCodeResponse authorizationCodeResponse = response.getEntity(AuthorizationCodeResponse.class);
		Assert.assertNotNull(authorizationCodeResponse.getCode());
		final TokenData<String> responseCode = tokenService.unmarshall(authorizationCodeResponse.getCode());
		Assert.assertNotNull(responseCode);
		Assert.assertEquals(accountId, responseCode.getAccountID());
		Assert.assertEquals(deviceSerialId, responseCode.getCid());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testGetAuthorizationCode_RemoteControlPlayBack_DongleNotRegistered() throws Exception {
		final IdentityServiceImpl identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		final OAuth2TokenService oAuth2TokenService = getBean("oAuth2TokenService", OAuth2TokenService.class);
		final SecureTokenService tokenService = getBean("simpleTokenService", SecureTokenService.class);
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		final TokenData accessTokenData = new TokenData();
		final String accountId = "accountId";
		final String profileId = "profileId";
		final String deviceSerialId = "deviceSerialId";
		final Date currentDate = new Date();
		
		assertNotNull(identityServiceImpl);
		assertNotNull(oAuth2TokenService);
		
		accessTokenData.setAccountID(accountId);
		accessTokenData.setProfileID(profileId);
		List<String> scopes = new ArrayList<String>();
		scopes.add("role_verified_identity");
		accessTokenData.setScope(scopes);
		accessTokenData.setCreationDate(currentDate);
		accessTokenData.setExpires_in(3600 * 8);  // 8 hours
		
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP.getLabel());
		authorizationCodeRequest.setAccessToken(tokenService.marshall(accessTokenData));
		authorizationCodeRequest.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setClientId(deviceSerialId);
		authorizationCodeRequest.setResponseType("code");
		
		mockAccountManagementGetDevice(identityServiceImpl, profileId, deviceSerialId, null);
		
		//
		// Call grizzly localhost
		//
		final String vuid = "VUID_" + currentDate.getTime();
		final  WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/authorization_code.json");
		
		final Builder builder = wr.header("x-mobitv-vuid", vuid);
		final ClientResponse response = builder.post(ClientResponse.class, authorizationCodeRequest);
		Assert.assertNotNull(response);
		Assert.assertEquals(Status.UNAUTHORIZED, response.getClientResponseStatus());
	}
	
	@Test
	public void getTokensWithAuthorizationCode_GuestPlayBackMode_DifferentAccountAsDeviceId_Success() throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();
		
		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.AUTHENTICATE_PLAYBACK;
		
		getTokensWithAuthorizationCodeSuccess(profileId, accountId, deviceId,
				accessTokenScope, authorizationCodeScope, null);
	}
	
	@Test
	public void getTokensWithAuthorizationCode_GuestPlayBackMode_SameAccountAsDeviceId_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.AUTHENTICATE_PLAYBACK;

		Device device = createDevice(deviceId);

		getTokensWithAuthorizationCodeSuccess(profileId, accountId, deviceId,
				accessTokenScope, authorizationCodeScope, device);
	}
	
	@Test
	public void getTokensWithAuthorizationCode_RemoteControlSetup_SameAccountAsDeviceId_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.REMOTE_CONTROL_SETUP;

		Device device = createDevice(deviceId);

		getTokensWithAuthorizationCodeSuccess(profileId, accountId, deviceId,
				accessTokenScope, authorizationCodeScope, device);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void getTokensWithAuthorizationCode_RemoteControlSetup_DeviceDeregisteredAfterGettingCode_Success() throws Exception {
		final IdentityServiceImpl identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();
		
		// mock account management
		mockAccountManagementGetDevice(identityServiceImpl, profileId, deviceId);
		
		// Generate access token
		final SecureTokenService tokenService = getBean("simpleTokenService", SecureTokenService.class);
		TokenData accessTokenData = new TokenData();
		accessTokenData.setAccountID(accountId);
		accessTokenData.setProfileID(profileId);
		accessTokenData.setCarrier(CARRIER);
		accessTokenData.setPrduct(PRODUCT);
		accessTokenData.setCreationDate(new Date());
		accessTokenData.setExpires_in(900);
		accessTokenData.setTokenType(OAuth2TokenType.access_token);
		List<String> scopes = new ArrayList<String>();
		scopes.add("role_verified_identity");
		accessTokenData.setScope(scopes);
		String accessToken = tokenService.marshall(accessTokenData);
		
		// Create authorization code request
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken(accessToken);
		authorizationCodeRequest.setClientId(deviceId);
		authorizationCodeRequest.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP.getLabel());
		
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class, authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.OK, response.getClientResponseStatus());
		final AuthorizationCodeResponse authorizationCodeResponse = response.getEntity(AuthorizationCodeResponse.class);
		String code = authorizationCodeResponse.getCode();
		assertNotNull(code);
		
		mockAccountManagementGetDevice(identityServiceImpl, profileId, deviceId, null);
		
		wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");
		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setCode(code);
		tokensRequest.setGrantType(GrantType.authorization_code.name());
		tokensRequest.setClientId(deviceId);
		
		ClientResponse clientResponse = wr.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.UNAUTHORIZED, clientResponse.getClientResponseStatus());
	}
	
	@Test
	public void getAuthorizationCode_GuestPlayBackMode_Error_MissingResponseType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_MissingResponseType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_Error_MissingScope()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_MissingGrantType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());
		authorizationCodeRequest.setResponseType("code");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_AuthenticatePlayback_Error_MissingGrantType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());
		authorizationCodeRequest.setResponseType("code");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_MissingScope()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_AuthenticatePlayback_Error_MissingClientId()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_MissingClientId()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_AuthenticatePlayback_Error_GrantTypeUnknown()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest
				.setGrantType("somegranttype");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_GrantTypeUnknown()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest
				.setGrantType("somegranttype");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_AuthenticatePlayback_Error_MissingAccessToken()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setGrantType("access_token");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_Error_InvalidScope()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setGrantType("access_token");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope("somescope");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_MissingAccessToken()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setGrantType("access_token");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_GuestPlayBackMode_Error_InvalidResponseType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setScope(ScopeType.AUTHENTICATE_PLAYBACK
				.getLabel());
		authorizationCodeRequest.setResponseType("someresponsetype");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getAuthorizationCode_RemoteControlSetupMode_Error_InvalidResponseType()
			throws Exception {
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken("someaccesstoken");
		authorizationCodeRequest.setClientId("somedeviceid");
		authorizationCodeRequest
				.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setScope(ScopeType.REMOTE_CONTROL_SETUP
				.getLabel());
		authorizationCodeRequest.setResponseType("someresponsetype");

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class,
				authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.BAD_REQUEST, response.getClientResponseStatus());

		ErrorWrapper errorWrapper = response.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}
	
	@Test
	public void getTokensWithAuthorizationCode_Error_MissingCode() throws Exception {
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");
		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setGrantType(GrantType.authorization_code.name());
		tokensRequest.setClientId("somedeviceid");
		
		ClientResponse clientResponse = wr.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST, clientResponse.getClientResponseStatus());
	}
	
	@Test
	public void getTokensWithAuthorizationCode_Error_MissingClientId() throws Exception {
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");
		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setCode("somecode");
		tokensRequest.setGrantType(GrantType.authorization_code.name());
		
		ClientResponse clientResponse = wr.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST, clientResponse.getClientResponseStatus());
	}
	
	@Test
	public void getTokensWithRefreshTokens_GuestPlayBackMode_SameAccountAsDeviceId_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.AUTHENTICATE_PLAYBACK;

		Device device = createDevice(deviceId);

		Token tokenWithAuthorizationCode = getTokensWithAuthorizationCodeSuccess(
				profileId, accountId, deviceId, accessTokenScope,
				authorizationCodeScope, device);

		String refreshToken = tokenWithAuthorizationCode.getRefreshToken();

		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setRefreshToken(refreshToken);
		tokensRequest.setGrantType(GrantType.refresh_token.toString());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/tokens.json");
		ClientResponse clientResponse = wr.header("Content-Type",
				MediaType.APPLICATION_JSON).post(ClientResponse.class,
				tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());

		Token tokenWithRefreshToken = clientResponse.getEntity(Token.class);
		assertNotNull(tokenWithRefreshToken);
		String accessTokenWithRefreshToken = tokenWithRefreshToken
				.getAccessToken();
		assertNotNull(accessTokenWithRefreshToken);
	}
	
	@Test
	public void getTokensWithRefreshTokens_GuestPlayBackMode_DifferentAccountAsDeviceId_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.AUTHENTICATE_PLAYBACK;

		Token tokenWithAuthorizationCode = getTokensWithAuthorizationCodeSuccess(
				profileId, accountId, deviceId, accessTokenScope,
				authorizationCodeScope, null);

		String refreshToken = tokenWithAuthorizationCode.getRefreshToken();

		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setRefreshToken(refreshToken);
		tokensRequest.setGrantType(GrantType.refresh_token.toString());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/tokens.json");
		ClientResponse clientResponse = wr.header("Content-Type",
				MediaType.APPLICATION_JSON).post(ClientResponse.class,
				tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());

		Token tokenWithRefreshToken = clientResponse.getEntity(Token.class);
		assertNotNull(tokenWithRefreshToken);
		String accessTokenWithRefreshToken = tokenWithRefreshToken
				.getAccessToken();
		assertNotNull(accessTokenWithRefreshToken);
	}
	
	@Test
	public void getTokensWithRefreshTokens_RemoteControlSetup_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.REMOTE_CONTROL_SETUP;

		Device device = createDevice(deviceId);

		Token tokenWithAuthorizationCode = getTokensWithAuthorizationCodeSuccess(
				profileId, accountId, deviceId, accessTokenScope,
				authorizationCodeScope, device);

		String refreshToken = tokenWithAuthorizationCode.getRefreshToken();

		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setRefreshToken(refreshToken);
		tokensRequest.setGrantType(GrantType.refresh_token.toString());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/tokens.json");
		ClientResponse clientResponse = wr.header("Content-Type",
				MediaType.APPLICATION_JSON).post(ClientResponse.class,
				tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());

		Token tokenWithRefreshToken = clientResponse.getEntity(Token.class);
		assertNotNull(tokenWithRefreshToken);
		String accessTokenWithRefreshToken = tokenWithRefreshToken
				.getAccessToken();
		assertNotNull(accessTokenWithRefreshToken);
	}
	
	@Test
	public void getTokensWithRefreshTokens_RemoteControlSetup_DeviceDeregisteredAfterGettingAccessToken_Success()
			throws Exception {
		// Generate account ID, profile ID and deviceID
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();
		String deviceId = UUID.randomUUID().toString();

		// Set scope
		String accessTokenScope = "role_verified_identity";
		ScopeType authorizationCodeScope = ScopeType.REMOTE_CONTROL_SETUP;

		Device device = createDevice(deviceId);

		Token tokenWithAuthorizationCode = getTokensWithAuthorizationCodeSuccess(
				profileId, accountId, deviceId, accessTokenScope,
				authorizationCodeScope, device);

		String refreshToken = tokenWithAuthorizationCode.getRefreshToken();

		// Deregister device
		final IdentityServiceImpl identityServiceImpl = getBean(
				"identityService", IdentityServiceImpl.class);
		mockAccountManagementGetDevice(identityServiceImpl, profileId,
				deviceId, null);

		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setRefreshToken(refreshToken);
		tokensRequest.setGrantType(GrantType.refresh_token.toString());

		WebResource wr = resource().path(
				BASE_URI_IDENTITY_SERVICE + "/" + CARRIER + "/" + PRODUCT + "/"
						+ VERSION + "/tokens.json");
		ClientResponse clientResponse = wr.header("Content-Type", MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.UNAUTHORIZED,
				clientResponse.getClientResponseStatus());
	}
	
	private void mockAccountManagementGetDevice(
	final IdentityServiceImpl identityServiceImpl,
	final String profileId, final String deviceSerialId) {
		mockAccountManagementGetDevice(identityServiceImpl, profileId,
				deviceSerialId, createDevice(deviceSerialId));
	}

	private void mockAccountManagementGetDevice(
			final IdentityServiceImpl identityServiceImpl,
			final String profileId, final String deviceSerialId, Device device) {
		final AccountManagerHelper mockAccountManagerHelper = Mockito.mock(AccountManagerHelper.class);
		Mockito
			.when(mockAccountManagerHelper.devices(Mockito.eq("mobitv"), Mockito.eq("reference"), Mockito.eq("5.0"), Mockito.eq(profileId), Mockito.eq(deviceSerialId), Mockito.anyString(), Mockito.anyString()))
			.thenReturn(device);
		identityServiceImpl.setAccountManagerHelper(mockAccountManagerHelper);
	}
	
	private void mockAccountManagementRegisterUser(
			final IdentityServiceImpl identityService,
			final String partnerProfileId, final String email,
			final Profile profile) {
		final AccountManagerHelper accountManagerHelper = Mockito
				.mock(AccountManagerHelper.class);
		Mockito.when(
				accountManagerHelper.registerUser(Mockito.eq("mobitv"),
						Mockito.eq("reference"), Mockito.eq("5.0"),
						Mockito.eq(partnerProfileId), Mockito.eq(email)))
				.thenReturn(profile);
		identityService.setAccountManagerHelper(accountManagerHelper);
	}

	private static Device createDevice(final String nativeDeviceId) {
		final Device device = new Device();
		
		device.setDeviceName("device_name");
		device.setDeviceType("device_type");
		device.setNativeDeviceId(nativeDeviceId);
		
		return device;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Token getTokensWithAuthorizationCodeSuccess(String profileId,
			String accountId, String deviceId, String accessTokenScope,
			ScopeType authorizationCodeScope, Device device) {
		final IdentityServiceImpl identityServiceImpl = getBean("identityService", IdentityServiceImpl.class);
		
		// mock account management
		mockAccountManagementGetDevice(identityServiceImpl, profileId, deviceId, device);
		
		// Generate access token
		final SecureTokenService tokenService = getBean("simpleTokenService", SecureTokenService.class);
		TokenData accessTokenData = new TokenData();
		accessTokenData.setAccountID(accountId);
		accessTokenData.setProfileID(profileId);
		accessTokenData.setCarrier(CARRIER);
		accessTokenData.setPrduct(PRODUCT);
		accessTokenData.setCreationDate(new Date());
		accessTokenData.setExpires_in(900);
		accessTokenData.setTokenType(OAuth2TokenType.access_token);
		List<String> scopes = new ArrayList<String>();
		scopes.add(accessTokenScope);
		accessTokenData.setScope(scopes);
		String accessToken = tokenService.marshall(accessTokenData);
		
		// Create authorization code request
		final AuthorizationCodeRequest authorizationCodeRequest = new AuthorizationCodeRequest();
		authorizationCodeRequest.setAccessToken(accessToken);
		authorizationCodeRequest.setClientId(deviceId);
		authorizationCodeRequest.setGrantType(GrantType.access_token.toString());
		authorizationCodeRequest.setResponseType("code");
		authorizationCodeRequest.setScope(authorizationCodeScope.getLabel());
		
		WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/authorization_code.json");
		ClientResponse response = wr.post(ClientResponse.class, authorizationCodeRequest);
		assertNotNull(response);
		assertEquals(Status.OK, response.getClientResponseStatus());
		final AuthorizationCodeResponse authorizationCodeResponse = response.getEntity(AuthorizationCodeResponse.class);
		String code = authorizationCodeResponse.getCode();
		assertNotNull(code);
		
		wr = resource().path(BASE_URI_IDENTITY_SERVICE
				+ "/" + CARRIER
				+ "/" + PRODUCT
				+ "/" + VERSION
				+ "/tokens.json");
		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setCode(code);
		tokensRequest.setGrantType(GrantType.authorization_code.name());
		tokensRequest.setClientId(deviceId);
		
		ClientResponse clientResponse = wr.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());
		
		Token token = clientResponse.getEntity(Token.class);
		assertNotNull(token);
		assertNotNull(token.getAccessToken());
		assertNotNull(token.getRefreshToken());
		
		return token;
	}
	
}
