/**
 * 
 */
package com.mobitv.aaa.oauth2.endpoint;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.platform.core.dto.ProfileResponse;
import com.mobitv.platform.identity.dto.Token;
import com.mobitv.platform.identity.dto.TokensRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;


public class SessionServiceTestCase extends JerseyTestBase {
	
	public SessionServiceTestCase() {
		super();
	}
	
	@Test
	@Ignore
	public void test_profile_GATEWAYHEADER() {
		
		long time = System.currentTimeMillis();
		
		String vuid	= "VUID_" + time;
		
		try {
			WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
										+ "/" + CARRIER
										+ "/" + PRODUCT
										+ "/" + VERSION
										+ "/tokens.json");
			
			Builder builder = wr.header("x-mobitv-vuid", vuid);

			TokensRequest tokenRequestEntity = new TokensRequest();
			tokenRequestEntity.setGrantType(GRANT_TYPE_GATEWAY_HEADER);
			tokenRequestEntity.setScope(DEFAULT_SCOPE);
			tokenRequestEntity.setClientId(String.valueOf(time));
			
			ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
			Token tokenResponseEntity = response.getEntity(Token.class);
			Assert.assertNotNull(tokenResponseEntity);
			Assert.assertNotNull(tokenResponseEntity.getAccessToken());
			Assert.assertEquals(DEFAULT_SCOPE, tokenResponseEntity.getScope());

			System.out.println(tokenResponseEntity.toString());
			
			wr = resource().path(BASE_URI_SESSION_SERVICE
					+ "/" + CARRIER
					+ "/" + PRODUCT
					+ "/" + VERSION
					+ "/current/profile.json");
			
			builder = wr.header(HttpHeaders.AUTHORIZATION, tokenResponseEntity.getTokenType() + " " + tokenResponseEntity.getAccessToken());
			
			response = builder.get(ClientResponse.class);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			ProfileResponse profileResponseEntity = response.getEntity(ProfileResponse.class);
			Assert.assertNotNull(profileResponseEntity);
			Assert.assertNotNull(profileResponseEntity.getProfileId());
			Assert.assertNotNull(profileResponseEntity.getAccountId());
			
			System.out.println(profileResponseEntity.toString());
		} catch (UniformInterfaceException e) {
			System.out.println("HTTP_STATUS = " + e.getResponse().getStatus());
			System.out.println("error_response = " + e.getResponse().getEntity(String.class));
			Assert.fail("UniformInterfaceException");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception");
		}
	}
	    
	@Test
	@Ignore
	public void test_profile_USERNAMEPASSWORD() {
		
		long time = System.currentTimeMillis();
		
		// NOTE: make sure the username, password are correctly configured
		String username	= "mobitest1";
		String password = "mobitest1";
		
		try {
			WebResource wr = resource().path(BASE_URI_IDENTITY_SERVICE
										+ "/" + CARRIER
										+ "/" + PRODUCT
										+ "/" + VERSION
										+ "/tokens.json");
			
			Builder builder = wr.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);

			TokensRequest tokenRequestEntity = new TokensRequest();
			tokenRequestEntity.setGrantType(GrantType.password.toString());
			tokenRequestEntity.setScope(DEFAULT_SCOPE);
			tokenRequestEntity.setClientId(String.valueOf(time));
			tokenRequestEntity.setUsername(username);
			tokenRequestEntity.setPassword(password);
			
			ClientResponse response = builder.post(ClientResponse.class, tokenRequestEntity);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			
			Token tokenResponseEntity = response.getEntity(Token.class);
			Assert.assertNotNull(tokenResponseEntity);
			Assert.assertNotNull(tokenResponseEntity.getAccessToken());

			System.out.println(tokenResponseEntity.toString());
			
			wr = resource().path(BASE_URI_SESSION_SERVICE
					+ "/" + CARRIER
					+ "/" + PRODUCT
					+ "/" + VERSION
					+ "/current/profile.json");
			
			builder = wr.header(HttpHeaders.AUTHORIZATION, tokenResponseEntity.getTokenType() + " " + tokenResponseEntity.getAccessToken());
			
			response = builder.get(ClientResponse.class);
			Assert.assertNotNull(response);
			Assert.assertEquals(Status.OK, response.getClientResponseStatus());
			ProfileResponse profileResponseEntity = response.getEntity(ProfileResponse.class);
			Assert.assertNotNull(profileResponseEntity);
			Assert.assertNotNull(profileResponseEntity.getProfileId());
			Assert.assertNotNull(profileResponseEntity.getAccountId());
			
			System.out.println(profileResponseEntity.toString());
		} catch (UniformInterfaceException e) {
			System.out.println("HTTP_STATUS = " + e.getResponse().getStatus());
			System.out.println("error_response = " + e.getResponse().getEntity(String.class));
			Assert.fail("UniformInterfaceException");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception");
		}
	}
	    
}
