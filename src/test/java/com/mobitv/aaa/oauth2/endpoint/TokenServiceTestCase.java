package com.mobitv.aaa.oauth2.endpoint;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Properties;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.authnz.dto.TokenInfoRequest;
import com.mobitv.exceptionwrapper.dto.ErrorWrapper;
import com.mobitv.platform.identity.dto.Token;
import com.mobitv.platform.identity.dto.TokensRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;

public class TokenServiceTestCase extends JerseyTestBase {

	private static final String CARRIER = "c";

	private static final String PRODUCT = "p";

	private static final String VERSION = "v";

	private static final String ACCESS_TOKEN_SCOPE = "role_verified_identity";

	public TokenServiceTestCase() {
		super();
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testVerify_Success() {
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();

		Token token = generateTokens(CARRIER, PRODUCT, VERSION, profileId,
				accountId, ACCESS_TOKEN_SCOPE);
		String accessToken = token.getAccessToken();

		ClientResponse clientResponse = verifyToken(CARRIER, PRODUCT, VERSION,
				accessToken);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());
		TokenData tokenData = clientResponse.getEntity(TokenData.class);
		assertNotNull(tokenData);
		assertEquals(OAuth2TokenType.access_token, tokenData.getTokenType());
		assertEquals(CARRIER, tokenData.getCarrier());
		assertEquals(PRODUCT, tokenData.getProduct());
		assertEquals(accountId, tokenData.getAccountID());
		assertEquals(profileId, tokenData.getProfileID());
		assertNotNull(tokenData.getScope());
		assertTrue(tokenData.getScope().contains(ACCESS_TOKEN_SCOPE));
	}

	@Test
	public void testVerify_Error_ExpiredAccessToken()
			throws InterruptedException {
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();

		Token token = generateTokens(CARRIER, PRODUCT, VERSION, profileId,
				accountId, ACCESS_TOKEN_SCOPE);
		String accessToken = token.getAccessToken();

		int accessTokenLifetimeSecs = getAccessTokenLifetimeSecs();
		Thread.sleep((accessTokenLifetimeSecs * 1000) + 100);

		ClientResponse clientResponse = verifyToken(CARRIER, PRODUCT, VERSION,
				accessToken);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST,
				clientResponse.getClientResponseStatus());
		ErrorWrapper errorWrapper = clientResponse
				.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}

	@Test
	public void testVerify_Error_IncorrectCarrier() {
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();

		Token token = generateTokens(CARRIER, PRODUCT, VERSION, profileId,
				accountId, ACCESS_TOKEN_SCOPE);
		String accessToken = token.getAccessToken();

		ClientResponse clientResponse = verifyToken("somecarrier", PRODUCT,
				VERSION, accessToken);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST,
				clientResponse.getClientResponseStatus());
		ErrorWrapper errorWrapper = clientResponse
				.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}

	@Test
	public void testVerify_Error_IncorrectProduct() {
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();

		Token token = generateTokens(CARRIER, PRODUCT, VERSION, profileId,
				accountId, ACCESS_TOKEN_SCOPE);
		String accessToken = token.getAccessToken();

		ClientResponse clientResponse = verifyToken(CARRIER, "someproduct",
				VERSION, accessToken);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST,
				clientResponse.getClientResponseStatus());
		ErrorWrapper errorWrapper = clientResponse
				.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}

	@Test
	public void testVerify_Error_RefreshTokenInsteadOfAccessToken() {
		String profileId = UUID.randomUUID().toString();
		String accountId = UUID.randomUUID().toString();

		Token token = generateTokens(CARRIER, PRODUCT, VERSION, profileId,
				accountId, ACCESS_TOKEN_SCOPE);
		String refreshToken = token.getRefreshToken();

		ClientResponse clientResponse = verifyToken(CARRIER, PRODUCT, VERSION,
				refreshToken);
		assertNotNull(clientResponse);
		assertEquals(Status.BAD_REQUEST,
				clientResponse.getClientResponseStatus());
		ErrorWrapper errorWrapper = clientResponse
				.getEntity(ErrorWrapper.class);
		assertNotNull(errorWrapper);
	}

	private Token generateTokens(String carrier, String product,
			String version, String profileId, String accountId, String scope) {
		TokensRequest tokensRequest = new TokensRequest();
		tokensRequest.setAccountId(accountId);
		tokensRequest.setProfileId(profileId);
		tokensRequest.setGrantType(GrantType.profile_id.toString());
		tokensRequest.setScope(scope);

		String tokensUrl = new StringBuilder("/identity/v5/oauth2/")
				.append(carrier).append("/").append(product).append("/")
				.append(version).append("/tokens.xml").toString();

		ClientResponse clientResponse = resource().path(tokensUrl)
				.header("Content-Type", "application/xml")
				.post(ClientResponse.class, tokensRequest);
		assertNotNull(clientResponse);
		assertEquals(Status.OK, clientResponse.getClientResponseStatus());
		Token token = clientResponse.getEntity(Token.class);
		assertNotNull(token);
		assertTrue(StringUtils.isNotEmpty(token.getAccessToken()));
		assertTrue(StringUtils.isNotEmpty(token.getRefreshToken()));

		return token;
	}

	private ClientResponse verifyToken(String carrier, String product,
			String version, String token) {
		String verifyTokenUrl = new StringBuilder("/platform/v5/oauth2/")
				.append(carrier).append("/").append(product).append("/")
				.append(version).append("/tokens/verify.xml").toString();

		TokenInfoRequest tokenInfoRequest = new TokenInfoRequest();
		tokenInfoRequest.setAccess_token(token);

		ClientResponse clientResponse = resource().path(verifyTokenUrl)
				.header("Content-Type", "application/xml")
				.post(ClientResponse.class, tokenInfoRequest);
		return clientResponse;
	}

	private int getAccessTokenLifetimeSecs() {
		Properties properties = getBean("appProperties", Properties.class);
		assertNotNull(properties);
		String accessTokenLifetimeSecs = (String) properties
				.get("access_token.lifetime.secs");
		assertNotNull(accessTokenLifetimeSecs);
		assertTrue(StringUtils.isNotEmpty(accessTokenLifetimeSecs));
		return Integer.parseInt(accessTokenLifetimeSecs);
	}

}
