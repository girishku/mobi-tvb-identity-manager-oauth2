package com.mobitv.aaa.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("assetService")
public class AssetPropertyHolderList {
	
	private @Value("${service_asset}") String[] serviceAssets;

	private List<AssetPropertyHolder> assets;
	
	public AssetPropertyHolderList() {
		this.assets = new ArrayList<AssetPropertyHolder>();
	}
	
	public AssetPropertyHolderList(List<AssetPropertyHolder> serviceAssets) {
		this.assets = serviceAssets;
	}
	
	@SuppressWarnings("unused")
	@PostConstruct
	private void init(){
		if(serviceAssets!=null)
			for(String stringAssests : serviceAssets){
				AssetPropertyHolder asset = new AssetPropertyHolder(stringAssests);
				this.assets.add(asset);
			}
	}

	public void setAssets(List<AssetPropertyHolder> assets) {
		this.assets = assets;
	}

	public List<AssetPropertyHolder> getAssets() {
		return assets;
	}

	
}
