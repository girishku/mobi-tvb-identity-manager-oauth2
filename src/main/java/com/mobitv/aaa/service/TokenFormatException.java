package com.mobitv.aaa.service;

import com.mobitv.exceptionwrapper.exception.MobiException;

@SuppressWarnings("serial")
public class TokenFormatException extends MobiException {

	public TokenFormatException(String errorCode, String message, String detail, int httpStatusCode) {
		super(errorCode, message, detail, httpStatusCode);
	}

	//400 bad request by default
	public TokenFormatException(String errorCode, String message, String detail) {
		super(errorCode, message, detail);
	}

	@Override
	public String toString() {
		return "TokenFormatException [getCode()=" + getExceptionWrapper().getCode()
				+ ", getMessage()=" + getMessage() + ", getDetail()="
				+ getExceptionWrapper().getDetail() + ", getHttpStatusCode()=" + getHttpStatusCode()
				+ "]";
	}
}
