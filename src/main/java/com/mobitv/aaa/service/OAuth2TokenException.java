package com.mobitv.aaa.service;

import com.mobitv.exceptionwrapper.exception.MobiException;

@SuppressWarnings("serial")
public class OAuth2TokenException extends MobiException {

	public OAuth2TokenException(String errorCode, String message, String detail) {
		super(errorCode, message, detail);
	}
}
