package com.mobitv.aaa.service;

import java.util.List;

import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.oauth2.endpoint.ScopeType;

public interface AuthorizationCodeService {

	public String create(String carrier, String product, String version,
			String profileId, String accountId, List<String> scopes,
			List<Statement> statements, String clientId, String sessionId, ScopeType scopeType);
	
	public TokenData getTokenData(String authorizationCode);

}
