package com.mobitv.aaa.service;

import org.apache.commons.lang.StringUtils;

/**
 * @author rdirisala
 *
 */
public class UserIdentityDTO {
	
	private String userName; 
	private String password;
	private String partnerProfileId;
	private String email;
	
	public UserIdentityDTO() { }
	
	public UserIdentityDTO(String username, String values) throws Exception {
		String[] tokens = values.split(",");
		if (tokens.length == 3 && StringUtils.isNotEmpty(tokens[0])
				&& StringUtils.isNotEmpty(tokens[1]) && StringUtils.isNotEmpty(tokens[2])) {
			this.userName = username;
			this.password = tokens[0];
			this.partnerProfileId = tokens[1];
			this.email = tokens[2];
		} else {
			throw new Exception();
		}
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the partnerProfileId
	 */
	public String getPartnerProfileId() {
		return partnerProfileId;
	}
	/**
	 * @param partnerProfileId the partnerProfileId to set
	 */
	public void setPartnerProfileId(String partnerProfileId) {
		this.partnerProfileId = partnerProfileId;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "UserIdentityDTO [userName=" + userName + ", password="
				+ password + ", partnerProfileId=" + partnerProfileId
				+ ", email=" + email + "]";
	}
	
}
