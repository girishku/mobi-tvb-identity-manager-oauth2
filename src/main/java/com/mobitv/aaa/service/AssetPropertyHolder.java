package com.mobitv.aaa.service;

public class AssetPropertyHolder {

	private final String name;
	private final String[] accepts;

	public AssetPropertyHolder(String name) {
		super();
		this.name = name;
		this.accepts = null;
	}
	
	public AssetPropertyHolder(String name, String[] accepts) {
		super();
		this.name = name;
		this.accepts = accepts;
	}

	public String getName() {
		return name;
	}

	public String[] getAccepts() {
		return accepts;
	}
}
