package com.mobitv.aaa.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.oauth2.endpoint.StatementNameEnum;
import com.mobitv.aaa.service.OAuth2TokenException;
import com.mobitv.aaa.service.OAuth2TokenService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.TokenFormatException;
import com.mobitv.commons.pii.util.Entity;
import com.mobitv.commons.pii.util.PIIEncrypter;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.platform.identity.dto.Token;

@Service("oAuth2TokenService")
public class OAuth2TokenServiceImpl implements OAuth2TokenService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Resource(name="specialScopes")
	private Map<String, String> specialScopes;
	
	SecureTokenService tokenService;
	
	@Autowired
	public void setTokenService(SecureTokenService tokenService) {
		this.tokenService = tokenService;
	}

	private @Value("${access_token.lifetime.secs}") int accessTokenLifeTime = 1800;	
	private @Value("${access_token_for_pin.lifetime.secs}") int accessTokenForPINLifeTime = 1800;
	private @Value("${refresh_token.lifetime.secs}") int refreshTokenLifeTime = 1209600;
			
	@Override
	public Token create(String carrier, String product, String profileID, String scope, boolean associateUser, String accountId) throws OAuth2TokenException, TokenFormatException {
		return create(carrier, product, profileID, scope, associateUser, accountId, null, null, false);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public <T> Token create(String carrier, String product, String profileID, String scope, boolean associateUser, String accountId, List<Statement> statements, T deviceId, boolean useAccessTokenForPINLifeTimeValue) throws OAuth2TokenException, TokenFormatException {
		if (StringUtils.isEmpty(profileID)) {
			throw new IllegalArgumentException("Profile can not be null");
		}

		Token returnToken = new Token();
		returnToken.setTokenType("Bearer");
		
		//access token first
		TokenData accessTokenData = new TokenData();
		accessTokenData.setTokenType(OAuth2TokenType.access_token);
		accessTokenData.setCreationDate(new Date());
		accessTokenData.setProfileID(profileID);
		String tempAccountId = StringUtils.remove(profileID, "-");
		tempAccountId = (tempAccountId.length()>=10)? tempAccountId.substring(0, 10) : tempAccountId;
		if(StringUtils.isBlank(accountId))
			accountId = (profileID.contains("anonymous"))? "anonymous-" + UUID.randomUUID().toString() : tempAccountId;
			
		accessTokenData.setAccountID(accountId);
		accessTokenData.setExpires_in(useAccessTokenForPINLifeTimeValue ? accessTokenForPINLifeTime : accessTokenLifeTime);
		accessTokenData.setCarrier(carrier);
		accessTokenData.setPrduct(product);
		
		//Scope is space delimited
		accessTokenData.setScope(Arrays.asList(StringUtils.split(scope)));
		
		logger.info("Generated access_token data " + accessTokenData);
		
		TokenData refreshTokenData = new TokenData();
		refreshTokenData.setTokenType(OAuth2TokenType.refresh_token);
		refreshTokenData.setCreationDate(new Date());
		refreshTokenData.setCarrier(carrier);
		refreshTokenData.setPrduct(product);
		refreshTokenData.setProfileID(profileID);
		refreshTokenData.setAccountID(accountId);
		refreshTokenData.setExpires_in(refreshTokenLifeTime);
		refreshTokenData.setScope(accessTokenData.getScope());
		logger.info("Generated refresh_token data " + refreshTokenData);
		
		if (statements != null && !statements.isEmpty()) {
			accessTokenData.setStatements(statements);
			refreshTokenData.setStatements(statements);
		}
		
		if (deviceId != null) {
			accessTokenData.setCid(deviceId);
			refreshTokenData.setCid(deviceId);
		}
		
		String access_token = tokenService.marshall(accessTokenData);
		String refresh_token = tokenService.marshall(refreshTokenData);
		returnToken.setRefreshToken(refresh_token);
		returnToken.setAccessToken(access_token);
		returnToken.setExpiresIn(useAccessTokenForPINLifeTimeValue ? accessTokenForPINLifeTime : accessTokenLifeTime);
		returnToken.setScope(scope);
		
		return returnToken;
	}
	
	@Override
	public Token create(String carrier, String product, 
			String profileID, String scope, 
			String clientId, String vuid, String alternateVuid, 
			String wapProfile, String userAgent, String nai, String accountId) throws OAuth2TokenException, TokenFormatException {
		
		if (StringUtils.isEmpty(vuid)) {
			throw new IllegalArgumentException("vuid can not be null");
		}
		
		if (StringUtils.isEmpty(profileID)) {
			PIIEncrypter piiEnc = new PIIEncrypter(Entity.CLEAR_TEXT_NAI);
			logger.info("generating encyrpted profileid based on " + vuid);
			profileID = piiEnc.encrypt(vuid);
			logger.info("generated profile id " + profileID);
		}
		
		List<Statement> statements = new ArrayList<Statement>();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, accessTokenLifeTime);
		
		// add the 'x-mobitv-vuid'
		Statement statement = new Statement();
		statement.setName(StatementNameEnum.x_mobitv_vuid.getStatementName());
		statement.setValue(vuid);
		statement.setExpirationTime(cal.getTime());
		statements.add(statement);

		// add the 'x-mobitv-alternate-vuid'
		if (StringUtils.isNotEmpty(alternateVuid)) {
			Statement stmt = new Statement();
			stmt.setName(StatementNameEnum.x_mobitv_alternate_vuid.getStatementName());
			stmt.setValue(alternateVuid);
			stmt.setExpirationTime(cal.getTime());
			statements.add(stmt);
		}

		// add the 'x-wap-profile'
		if (StringUtils.isNotEmpty(wapProfile)) {
			Statement stmt = new Statement();
			stmt.setName(StatementNameEnum.x_wap_profile.getStatementName());
			stmt.setValue(wapProfile);
			stmt.setExpirationTime(cal.getTime());
			statements.add(stmt);
		}
		
		// add the 'user-agent'
		if (StringUtils.isNotEmpty(userAgent)) {
			Statement stmt = new Statement();
			stmt.setName(StatementNameEnum.user_agent.getStatementName());
			stmt.setValue(userAgent);
			stmt.setExpirationTime(cal.getTime());
			statements.add(stmt);
		}
		
		// add the 'x-mobitv-nai'
		if (StringUtils.isNotEmpty(nai)) {
			Statement stmt = new Statement();
			stmt.setName(StatementNameEnum.x_mobitv_nai.getStatementName());
			stmt.setValue(nai);
			stmt.setExpirationTime(cal.getTime());
			statements.add(stmt);
		}
		
		return create(carrier, product, profileID, scope, false, accountId, statements, clientId, false);
	}
	
    public Token associateUser(String carrier, String product, String profileID, String scope, String accessToken) throws OAuth2TokenException, TokenFormatException {
		
		if (StringUtils.isEmpty(profileID)) {
			throw new IllegalArgumentException("Profile can not be null");
		}
		
		TokenData data = tokenService.unmarshall(accessToken);
		ArrayList<Statement> statementsToAdd = new ArrayList<Statement>();
		
		if (data.getStatements()!=null)
		{
			List<Statement> stmts = data.getStatements();
			for (Statement statement : stmts)
			{
				if (statement.getExpirationTime().after(new Date()) && !StringUtils.equals(statement.getName(), "associated_profile"))
					statementsToAdd.add(statement);
			}
		}
		Statement newStatement = new Statement();
		newStatement.setExpirationTime(new Date(System.currentTimeMillis()+accessTokenLifeTime*1000));
		newStatement.setName("associated_profile");
		newStatement.setValue(profileID);
		statementsToAdd.add(newStatement);
		data.setStatements(statementsToAdd);
		data.setCreationDate(new Date());
		data.setExpires_in(accessTokenLifeTime);
		data.setScope(Arrays.asList(StringUtils.split(scope)));
		
		Token returnToken = new Token();
		returnToken.setTokenType("Bearer");
		
				
		logger.info("Generated access_token data " + data);
		
		String access_token = tokenService.marshall(data);
		
		returnToken.setAccessToken(access_token);
		returnToken.setExpiresIn(accessTokenLifeTime);
		returnToken.setScope(scope);
		
		
		TokenData refreshTokenData = new TokenData();
		refreshTokenData.setTokenType(OAuth2TokenType.refresh_token);
		refreshTokenData.setCreationDate(new Date());
		refreshTokenData.setCarrier(carrier);
		refreshTokenData.setPrduct(product);
		refreshTokenData.setProfileID(data.getProfileID());
		refreshTokenData.setAccountID(data.getAccountID());
		refreshTokenData.setExpires_in(refreshTokenLifeTime);
		refreshTokenData.setScope(data.getScope());
		refreshTokenData.setStatements(statementsToAdd);
		logger.info("Generated refresh_token data " + refreshTokenData);
		String refresh_token = tokenService.marshall(refreshTokenData);
		returnToken.setRefreshToken(refresh_token);
		
		return returnToken;
	
	}
	
	@Override
	public Token renew(String carrier, String product, String refreshToken) throws OAuth2TokenException {

		logger.debug("checking if the refresh_token is still valid");
		TokenData tokenData = tokenService.unmarshall(refreshToken);
		
		logger.debug("Attempting to renew " + tokenData);
		if (tokenData.getTokenType().equals(OAuth2TokenType.refresh_token)) {
			
			//Is it still valid?
			if (!this.isValid(tokenData)) {
				logger.info("refresh_token has expired.");
				throw new MobiException("IDM2001", "Forbidden", "refresh_token has expired!", 403);
			}
			
			logger.debug("Generating new tokens");
			Token returnToken = create(tokenData.getCarrier(), tokenData.getProduct(), tokenData.getProfileID(), StringUtils.join(tokenData.getScope(), " "), false, tokenData.getAccountID(), tokenData.getStatements(), tokenData.getCid(), false);
			
			return returnToken;
		}
		else {
			logger.warn("Invaild token type : expected refresh_token");
			throw new OAuth2TokenException("IDM2002", "invaild_grant", "Invaild token type : expected refresh_token");
		}
	}

	
	@Override
	public Token exchange(String carrier, String product, String accessToken, String scope) throws OAuth2TokenException {
		
		logger.debug("checking if the access_token is still valid");
		TokenData tokenData = tokenService.unmarshall(accessToken);
		
		logger.debug("Attempting to renew " + tokenData);
		if (tokenData.getTokenType().equals(OAuth2TokenType.access_token)) {
			
			//Is it still valid?
			if (!this.isValid(tokenData)) {
				logger.info("refresh_token has expired.");
				throw new MobiException("IDM2001", "Forbidden", "access_token has expired!", 403);	
			}
			
			//Only one item in scope is supported
			if (StringUtils.split(scope).length > 1) {
				throw new OAuth2TokenException("IDM2002", "invaild_scope", "Only single scope is allowed in token_for_token");
			}
			
			logger.debug("Generating new tokens");
			
			Token returnToken = new Token();
			if (specialScopes.containsKey(scope)) {
				returnToken.setTokenType(specialScopes.get(scope));
			}
			else
				returnToken.setTokenType("Bearer");
			
			
			//access token first
			TokenData accessTokenData = new TokenData();
			accessTokenData.setTokenType(OAuth2TokenType.access_token);
			accessTokenData.setCreationDate(tokenData.getCreationDate());
			accessTokenData.setCarrier(carrier);
			accessTokenData.setPrduct(product);			
			accessTokenData.setProfileID(tokenData.getProfileID());
			String accountId = (tokenData.getProfileID().contains("anonymous"))? "anonymous-" + UUID.randomUUID().toString() : tokenData.getProfileID();
			accessTokenData.setAccountID(accountId);
			accessTokenData.setExpires_in(tokenData.getExpires_in());
			

			accessTokenData.setScope(Arrays.asList(scope));
			
			logger.info("Generated access_token data " + accessTokenData);
			
			String access_token = tokenService.marshall(accessTokenData);
			
			returnToken.setAccessToken(access_token);
			returnToken.setExpiresIn(tokenData.getExpires_in());
			returnToken.setScope(scope);
			
			logger.info("Generated access_token data " + accessTokenData);
			
			return returnToken;
		}
		else {
			logger.warn("Invaild token type : expected access_token");
			throw new OAuth2TokenException("IDM2002", "invaild_grant", "Invaild token type : expected access_token");
		}
	}

	@Override
	public boolean isValid(TokenData token) {
		
		Date currentdate = new Date();
		
		//Created after && not expired
		if (currentdate.after(token.getCreationDate())) {
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(token.getCreationDate());
			cal.add(Calendar.SECOND, token.getExpires_in());
			
			if (currentdate.before(cal.getTime()))
				return true;
			else 
				return false;
		}
		else {
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Token create(TokenData tokenData) throws OAuth2TokenException,
			TokenFormatException {
		Date creationDate = new Date();
		
		TokenData accessTokenData = new TokenData();
		accessTokenData.setTokenType(OAuth2TokenType.access_token);
		accessTokenData.setCreationDate(creationDate);
		accessTokenData.setProfileID(tokenData.getProfileID());
		accessTokenData.setAccountID(tokenData.getAccountID());
		accessTokenData.setExpires_in(accessTokenLifeTime);
		accessTokenData.setCarrier(tokenData.getCarrier());
		accessTokenData.setPrduct(tokenData.getProduct());
		
		Date statementsExpiresDate = DateUtils.addSeconds(creationDate, accessTokenLifeTime);
		List<Statement> statements = new ArrayList<Statement>((List<Statement>) tokenData.getStatements());
		for (Statement statement : statements) {
			statement.setExpirationTime(statementsExpiresDate);
		}
		
		accessTokenData.setStatements(statements);
		accessTokenData.setCid(tokenData.getCid());
		accessTokenData.setScope(tokenData.getScope());
		
		logger.info("Generated access_token data " + accessTokenData);
		String accessToken = tokenService.marshall(accessTokenData);
		
		TokenData refreshTokenData = new TokenData();
		refreshTokenData.setTokenType(OAuth2TokenType.refresh_token);
		refreshTokenData.setCreationDate(new Date());
		refreshTokenData.setCarrier(tokenData.getCarrier());
		refreshTokenData.setPrduct(tokenData.getProduct());
		refreshTokenData.setProfileID(tokenData.getProfileID());
		refreshTokenData.setAccountID(tokenData.getAccountID());
		refreshTokenData.setExpires_in(refreshTokenLifeTime);
		refreshTokenData.setScope(tokenData.getScope());
		refreshTokenData.setStatements(statements);
		refreshTokenData.setCid(tokenData.getCid());
		
		logger.info("Generated refresh_token data " + refreshTokenData);
		String refreshToken = tokenService.marshall(refreshTokenData);
		
		Token token = new Token();
		token.setAccessToken(accessToken);
		token.setRefreshToken(refreshToken);
		token.setScope(StringUtils.join(tokenData.getScope(), " "));
		token.setExpiresIn(accessTokenLifeTime);
		token.setTokenType("Bearer");
		
		return token;
	}
}
