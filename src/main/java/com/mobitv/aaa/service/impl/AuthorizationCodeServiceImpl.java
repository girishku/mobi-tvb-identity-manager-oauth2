package com.mobitv.aaa.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.oauth2.endpoint.ScopeType;
import com.mobitv.aaa.service.AuthorizationCodeService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.TokenFormatException;

@Service("authorizationCodeService")
public class AuthorizationCodeServiceImpl implements AuthorizationCodeService {
	
	@Autowired
	private SecureTokenService secureTokenService;
	
	private @Value("${authorization_code.lifetime.secs}") int authorizationCodeLifeTime = 30;
	
	public void setSecureTokenService(SecureTokenService secureTokenService) {
		this.secureTokenService = secureTokenService;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String create(String carrier, String product, String version,
			String profileId, String accountId, List<String> scopes,
			List<Statement> statements, String clientId, String sessionId, ScopeType scopeType) {
		TokenData authorizationCodeData = new TokenData();
		authorizationCodeData.setTokenType(OAuth2TokenType.authorization_code);
		Date creationDate = new Date();
		authorizationCodeData.setCreationDate(creationDate);
		authorizationCodeData.setProfileID(profileId);
		authorizationCodeData.setCid(clientId);
		String tempAccountId = StringUtils.remove(profileId, "-");
		tempAccountId = (tempAccountId.length() >= 10) ? tempAccountId
				.substring(0, 10) : tempAccountId;
		if (StringUtils.isBlank(accountId))
			accountId = (profileId.contains("anonymous")) ? "anonymous-"
					+ UUID.randomUUID().toString() : tempAccountId;
			
		authorizationCodeData.setAccountID(accountId);
		authorizationCodeData.setExpires_in(authorizationCodeLifeTime);
		authorizationCodeData.setCarrier(carrier);
		authorizationCodeData.setPrduct(product);
		
		if (scopes == null) {
			scopes = new ArrayList<String>();
		}
		scopes.add(scopeType.getLabel());
		authorizationCodeData.setScope(scopes);
		
		if (statements == null) {
			statements = new ArrayList<Statement>();
		}
		Statement statement = new Statement();
		statement.setName("x-mobitv-sid");
		statement.setValue(sessionId);
		statement.setExpirationTime(DateUtils.addSeconds(creationDate,
				authorizationCodeLifeTime));
		statements.add(statement);
		
		authorizationCodeData.setStatements(statements);
		
		return secureTokenService.marshall(authorizationCodeData);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public TokenData getTokenData(String authorizationCode) {
		try {
			return secureTokenService.unmarshall(authorizationCode);
		} catch (TokenFormatException e) {
			// Added for USR-3034
			return null;
		}
	}
}
