package com.mobitv.aaa.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mobitv.aaa.commons.jersey.restclient.JerseyRESTClient;
import com.mobitv.exceptionwrapper.dto.ErrorWrapper;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.exceptionwrapper.exception.MobiInternalServerErrorException;
import com.mobitv.platform.core.dto.Device;
import com.mobitv.platform.core.dto.DeviceList;
import com.mobitv.platform.core.dto.Profile;
import com.mobitv.platform.core.dto.ProfilePin;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;

/**
 * All access to Account Management should go through this helper class
 * 
 * @author rdirisala
 * @author jau
 */
@Component
public class AccountManagerHelper {
	
	private static Logger logger = LoggerFactory.getLogger(AccountManagerHelper.class);
	
	@Value("${account.manager.url}")
	private String baseURL;
	
	@Resource
	private JerseyRESTClient jerseyRESTClient;
	
	private static final Map<String, String> STANDARD_REQ_HEADER = new HashMap<String, String>();
	static {
		STANDARD_REQ_HEADER.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
		STANDARD_REQ_HEADER.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
	}
	
	public Profile registerUser(String carrier, String product, String version, String partnerProfileId, String email) {

		StringBuilder buffer = new StringBuilder(baseURL).append("core/v5/accounts/").append(carrier).append("/").append(product).append("/")
				.append(version).append("/profiles.json");

		Profile profile = new Profile();

		profile.setPartnerProfileId(partnerProfileId);
		profile.setEmail(email);

		long start = System.currentTimeMillis();
		
		logger.info("registering account for user, partner_profile_id={} and email={}", partnerProfileId, email);

		try {
			final ClientResponse cr = jerseyRESTClient.doPOST(buffer.toString(), STANDARD_REQ_HEADER, profile);

			if (cr.getStatus() >= 300) {
				final ErrorWrapper ew = cr.getEntity(ErrorWrapper.class);
				final String errorMsg = String.format("Call to downstream service %s failed - code: %s reason: %s", buffer.toString(), ew.getCode(), ew.getDetail());
				
				// Fatal
				logger.error(errorMsg, cr);
				throw new RuntimeException(errorMsg);
			}
			
			return cr.getEntity(Profile.class);
		} finally {
			long end = System.currentTimeMillis();
			logger.debug("ACCOUNTMANAGEMENT_REGISTER-ACCOUNT_API took {}ms", (end - start));
		}
	}
	
	/**
	 * Returns the {@link Device} record for the given device serial ID and access token.  The
	 * result might be <code>null</code> if no match found.
	 * 
	 * @param carrier
	 * @param product
	 * @param version
	 * @param profileId
	 * @param deviceSerialId
	 * @param accessToken
	 * @param sessionId
	 * @return  the matching device record or <code>null</code> if no match found
	 */
	@SuppressWarnings("serial")
	public Device devices(
				final String carrier,
				final String product,
				final String version,
				final String profileId,
				final String deviceSerialId,
				final String accessToken,
				final String sessionId
			) {
		final StringBuilder url = new StringBuilder(baseURL)
				.append(String.format("core/v5/accounts/%s/%s/%s/%s/devices.json", carrier, product, version, profileId))
				.append("?native_device_id=").append(deviceSerialId);
		
		long start = System.currentTimeMillis();
		
		logger.info("Calling account management via url: {}", url.toString());

		try {
			final ClientResponse cr = jerseyRESTClient.doGET(
					url.toString(), 
					new HashMap<String, String>(STANDARD_REQ_HEADER) {{
						put("Authorization", "Bearer ".concat(accessToken));
						
						if (StringUtils.isNotBlank(sessionId)) {
							put("x-mobitv-sid", sessionId);
						}
					}});
			
			if (cr.getStatus() >= 300) {
				final ErrorWrapper ew = cr.getEntity(ErrorWrapper.class);
				// Account management returns error code AM-106 when a device is not registered to an account and AM-103 if the auth_token is invalid.
				// Either of these cases should return null to indicate an "unauthorized" state was encountered by downstream service which means the
				// original call is also unauthorized.
				if ("AM-106".equals(ew.getCode()) || "AM-103".equals(ew.getCode())) {
					return null;
				} 
				final String errorMsg = String.format("Call to downstream service %s failed - code: %s reason: %s", url.toString(), ew.getCode(), ew.getDetail());

				// Fatal
				logger.error(errorMsg, cr);
				throw new RuntimeException(errorMsg);
			}
			
			// Deserialize result
			final DeviceList deviceList = cr.getEntity(DeviceList.class);
			
			// Expecting exactly 0 or 1 device records to be returned since the query was made
			// using a device serial ID
			if (deviceList != null && CollectionUtils.isNotEmpty(deviceList.getDevices())) {
				return deviceList.getDevices().get(0);
			} else {
				return null;
			}
		} finally {
			long end = System.currentTimeMillis();
			logger.debug("ACCOUNTMANAGEMENT_LIST-DEVICES_API took {}ms", (end - start));
		}
	}
	
	/**
	 * Posts the given {@link Device} object as a new device for the given user
	 * 
	 * @param device
	 * @param carrier
	 * @param product
	 * @param version
	 * @param profileId
	 * @param deviceSerialId
	 * @param accessToken
	 * @param sessionId
	 */
	@SuppressWarnings("serial")
	public void addDevice(
			final Device device,
			final String carrier,
			final String product,
			final String version,
			final String profileId,
			final String deviceSerialId,
			final String accessToken,
			final String sessionId
			) {
		final StringBuilder url = new StringBuilder(baseURL)
		.append(String.format("core/v5/accounts/%s/%s/%s/%s/devices.json", carrier, product, version, profileId))
		.append("?native_device_id=").append(deviceSerialId);

		long start = System.currentTimeMillis();
		
		logger.info("Calling account management via url: {}", url.toString());
		
		try {
			final ClientResponse cr = jerseyRESTClient.doPOST(
					url.toString(), 
					new HashMap<String, String>(STANDARD_REQ_HEADER) {{
						put("Authorization", "Bearer ".concat(accessToken));
						
						if (StringUtils.isNotBlank(sessionId)) {
							put("x-mobitv-sid", sessionId);
						}
					}},
					device
					);
		
			if (cr.getStatus() >= 300) {
				final ErrorWrapper ew = cr.getEntity(ErrorWrapper.class);
				final String errorMsg = String.format("Jersey REST client error. code: %s reason: %s", ew.getCode(),ew.getDetail());
				
				// Fatal
				logger.error(errorMsg, cr);
				throw new RuntimeException(errorMsg);
			}			
		} finally {
			long end = System.currentTimeMillis();
			logger.debug("ACCOUNTMANAGEMENT_ADD-DEVICE_API took {}ms", (end - start));
		}
	}
	
	@SuppressWarnings("serial")
	public Profile validatePin(
			final String carrier,
			final String product,
			final String version,
			final String profileId,
			final String pin) {
		
		logger.info("validating PIN against account management");
		
		// /core/v5/accounts/{carrier}/{product}/{version}/{profileIdToValidate}/pin/validate.{format}
		String url = baseURL + "core/v5/accounts"
						+ "/" + carrier
						+ "/" + product
						+ "/" + version
						+ "/" + profileId
						+ "/pin/validate.json";
		
		ProfilePin profilePin = new ProfilePin();
		profilePin.setPin(pin);

		long start = System.currentTimeMillis();
		
		logger.info("Calling account-management via url: {}", url.toString());
		
		try {
			final ClientResponse cr = jerseyRESTClient.doPOST(
					url, 
					new HashMap<String, String>(STANDARD_REQ_HEADER) {{}},
					profilePin
					);
		
			int status = cr.getStatus();
			
			if (status == Status.OK.getStatusCode()) {
				return cr.getEntity(Profile.class);
			} else {
				try {
					ErrorWrapper ew = cr.getEntity(ErrorWrapper.class);
					logger.error("failed response from account-management:{}", ew.toString());
					throw new MobiException(status, ew);
				} catch (MobiException e) {
					throw e;
				} catch (Exception e) {
 					logger.error("Call to account-management failed with status={}", status);
					throw new MobiInternalServerErrorException("IDM5002", "INTERNAL_SERVER_ERROR", "Call to account-management failed with status=" + status);
				}
			}
		} finally {
			long end = System.currentTimeMillis();
			logger.debug("ACCOUNTMANAGEMENT_VALIDATE-PIN_API took {}ms", (end - start));
		}
		
	}
}
