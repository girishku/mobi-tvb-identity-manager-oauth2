package com.mobitv.aaa.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.TokenFormatException;

@Service("simpleTokenService")
public class SimpleTokenService implements SecureTokenService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ObjectMapper mapper = null;
	
	@PostConstruct
	public void init() throws Exception {
		
		mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, false);
		mapper.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, false);

		//Support JAXB annotations
		AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		mapper.setSerializationConfig(mapper.getSerializationConfig().withAnnotationIntrospector(introspector));
		mapper.setDeserializationConfig(mapper.getDeserializationConfig().withAnnotationIntrospector(introspector));		
	}
	
	@Override
	public String marshall(TokenData token) throws TokenFormatException {
		
		if (token == null)
			throw new IllegalArgumentException("token should not be null");
		
		logger.debug("Genreating " + token.getTokenType() + " from " + token.toString());
		
		String tokenData = new String();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			mapper.writeValue(os, token);
			tokenData = new String(Base64.encodeBase64(os.toByteArray()));
			
		} catch (JsonGenerationException e) {
			
			logger.error("Exception while genrating token " + e.getMessage());
			throw new TokenFormatException("IDM1001","Invaild token format", e.getMessage());
			
		} catch (JsonMappingException e) {
			
			logger.error("Exception while genrating token " + e.getMessage());
			throw new TokenFormatException("IDM1002", "Internal server error", e.getMessage(), 500);
			
		} catch (IOException e) {
			
			logger.error("Exception while genrating token " + e.getMessage());
			throw new TokenFormatException("IDM1001", "Internal server error", e.getMessage(), 500);
		}
		
		logger.debug("Generated " + tokenData);
		return tokenData;
	}

	@Override
	public TokenData unmarshall(String token) throws TokenFormatException {
		
		if (StringUtils.isEmpty(token)) {
			throw new IllegalArgumentException("Tokendata should not be empty");
		}
		
		logger.debug("Parsing token object from " + token);
		
		String jsonData = new String(Base64.decodeBase64(token.getBytes()));
		
		TokenData tokenData;
		try {
			tokenData = mapper.readValue(new ByteArrayInputStream(jsonData.getBytes()), TokenData.class);
		} catch (JsonParseException e) {
			
			logger.error("Exception while parsing token " + e.getMessage());
			throw new TokenFormatException("IDM1001","Invaild token format", e.getMessage());
			
		} catch (JsonMappingException e) {
			
			logger.error("Exception while parsing token " + e.getMessage());
			throw new TokenFormatException("IDM1002", "Internal server error", e.getMessage(), 500);
			
		} catch (IOException e) {
			logger.error("Exception while parsing token " + e.getMessage());
			throw new TokenFormatException("IDM1002", "Internal server error", e.getMessage(), 500);
		}
		
		logger.debug("Generated " + tokenData);
		
		return tokenData;
	}
}
