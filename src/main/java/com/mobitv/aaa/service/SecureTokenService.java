package com.mobitv.aaa.service;

import com.mobitv.aaa.authnz.dto.TokenData;

public interface SecureTokenService {
	
	public String marshall(TokenData token) throws TokenFormatException;
	public TokenData unmarshall(String token) throws TokenFormatException;
	
}
