package com.mobitv.aaa.service;

import java.util.List;

import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.platform.identity.dto.Token;

public interface OAuth2TokenService {

	/**
	 * Create a new OAuth2Token pair
	 * @param carrier TODO
	 * @param product TODO
	 * @param scope
	 * @param associateUser TODO
	 * @param accountId TODO
	 * @param profile_id
	 * @return
	 * @throws OAuth2TokenException
	 */
	public com.mobitv.platform.identity.dto.Token create(String carrier, String product, String profileID, String scope, boolean associateUser, String accountId) throws OAuth2TokenException;

	public com.mobitv.platform.identity.dto.Token create(String carrier, String product, 
			String profileID, String scope, 
			String clientId, String vuid, String alternateVuid,
			String wapProfile, String userAgent, String nai, String accountId) throws OAuth2TokenException, TokenFormatException;
	
	public com.mobitv.platform.identity.dto.Token create(TokenData tokenData) throws OAuth2TokenException, TokenFormatException;
	
	/**
	 * Renew a refresh_token for a new set of tokens
	 * @param carrier TODO
	 * @param product TODO
	 * @param refreshToken
	 * 
	 * @return
	 * @throws OAuth2TokenException
	 */
	public com.mobitv.platform.identity.dto.Token renew(String carrier, String product, String refreshToken) throws OAuth2TokenException;
	
	
	/**
	 * Exchange a token for another token
	 * @param carrier TODO
	 * @param product TODO
	 * @param accessToken
	 * @param scope
	 * 
	 * @return
	 * @throws OAuth2TokenException
	 */
	public com.mobitv.platform.identity.dto.Token exchange(String carrier, String product, String accessToken, String scope) throws OAuth2TokenException;

	public com.mobitv.platform.identity.dto.Token associateUser(String carrier, String product, String profileID, String scope, String accessToken) throws OAuth2TokenException, TokenFormatException;
	/**
	 * 
	 * @param data
	 * @return
	 */
	public boolean isValid(TokenData data);

	public <T> Token create(String carrier, String product, String profileID, String scope, boolean associateUser, String accountId, List<Statement> statements, T deviceId, boolean useAccessTokenForPINLifeTimeValue) throws OAuth2TokenException, TokenFormatException;
}
