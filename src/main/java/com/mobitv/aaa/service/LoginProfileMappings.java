package com.mobitv.aaa.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service("LoginProfileMappings")
public class LoginProfileMappings {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginProfileMappings.class);

	@Value("${login.to.profile.mapping.file}")
	private String loginToProfileMappingFile;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	private Properties defaultIdentities;
	
	@Autowired
	private Properties additionalIdentities;
	
	private Map<String, UserIdentityDTO> loginPrifleMapping = new HashMap<String, UserIdentityDTO>();
	
	@PostConstruct
	public void init() {
		//loading default identities
		Enumeration<Object> keys = defaultIdentities.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = defaultIdentities.getProperty(key);
			try {
				loginPrifleMapping.put(key, new UserIdentityDTO(key, value));
			} catch (Exception e) {
				LOGGER.warn("ingnoing invalid user identity:{}", value);
			}
		}
		
		//loading/merging additional identities
		keys = additionalIdentities.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = additionalIdentities.getProperty(key);
			try {
				loginPrifleMapping.put(key, new UserIdentityDTO(key, value));
			} catch (Exception e) {
				LOGGER.warn("ingnoing invalid user identity:{}", value);
			}
		}
		
		LOGGER.info("Found {} user identities, loaded {}", (defaultIdentities.size() + additionalIdentities.size()), loginPrifleMapping.size());
	}
	
	/**
	 * @param username
	 * @return
	 */
	public UserIdentityDTO getProfileForUser(String username) {
		return loginPrifleMapping.get(username);
	}
	
}
