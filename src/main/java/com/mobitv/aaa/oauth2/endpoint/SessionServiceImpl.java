package com.mobitv.aaa.oauth2.endpoint;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.service.OAuth2TokenException;
import com.mobitv.aaa.service.OAuth2TokenService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.TokenFormatException;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.exceptionwrapper.exception.MobiInternalServerErrorException;
import com.mobitv.platform.core.dto.ProfileResponse;
import com.mobitv.platform.core.rest.SessionService;
import com.sun.jersey.spi.resource.Singleton;

@Singleton
@Service("currentProfileService")
public class SessionServiceImpl implements SessionService {

	private final Pattern BEARER_SCHEME = Pattern.compile("Bearer (.+)");
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired	
	SecureTokenService tokenService;

	@Autowired
	OAuth2TokenService tokenFactory;

	public ProfileResponse getCurrentProfile(
			String carrier,
			String product,
			String version,
			String authzHeader,
			String accessToken
			)  {
		
		if (StringUtils.isEmpty(authzHeader) && StringUtils.isEmpty(accessToken)) {
			logger.warn("Authorization header is empty : returning forbidden");
			throw new MobiException("IDM3001", "Forbidden", "Authorization header is in incorrect format", 403);
		}
		
		//Parse the authz header
		if (StringUtils.isEmpty(accessToken)) {
			logger.info("Received getCurrentProfile request : Authorization: " + authzHeader);
			authzHeader = authzHeader.trim();
			Matcher m = BEARER_SCHEME.matcher(authzHeader);
			if (m.matches()) {
				//Extract the token
				accessToken = m.group(1);
			}
			else {
				logger.warn("Authtorization header is invalid format : returning forbidden");
				throw new MobiException("IDM3001", "Forbidden", "Authorization header is in incorrect format", 403);
			}
		}
		else {
			logger.info("Received getCurrentProfile request : access_token: " + accessToken);
		}
		
	
		// Attempt to read the token's contents.  
		TokenData oauth2Token;
		try {
			oauth2Token = tokenService.unmarshall(accessToken);
		} catch (TokenFormatException e) {
			// If fail to read then return 401 (USR-3137)
			throw new MobiException("IDM3001", e.getExceptionWrapper().getMessage(), e.getExceptionWrapper().getDetail(), 401);
		}
		
		//is it an access_token ?
		if (!oauth2Token.getTokenType().equals(OAuth2TokenType.access_token)) {
			
			logger.warn("Invalid token type: expected access_token");
			throw new OAuth2TokenException("IDM2003", "invaild_request", "expected access_token");
		}
		
		if (!tokenFactory.isValid(oauth2Token)) {
			logger.info("refresh_token has expired.");
			throw new MobiException("IDM2001", "Forbidden", "access_token has expired!", 403);	
		}
		
		ProfileResponse currentProfile = new ProfileResponse();
		currentProfile.setProfileId(oauth2Token.getProfileID());
		currentProfile.setAccountId(oauth2Token.getAccountID());
		
		logger.debug("Returning " + currentProfile);
//		return Response.ok(currentProfile).build();	
		return currentProfile;
	}
}
