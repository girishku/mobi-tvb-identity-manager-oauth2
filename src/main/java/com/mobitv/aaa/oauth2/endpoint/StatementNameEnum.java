package com.mobitv.aaa.oauth2.endpoint;

public enum StatementNameEnum {

	x_mobitv_vuid("x-mobitv-vuid"),
	
	x_mobitv_alternate_vuid("x-mobitv-alternate-vuid"),
	
	x_wap_profile("x-wap-profile"),
	
	user_agent("User-Agent"),
	
	x_mobitv_nai("x-mobitv-nai"),
	
	;
	
	private String statementName;
	
	private StatementNameEnum(String statementName) {
		this.statementName = statementName;
	}
	
	public String getStatementName() {
		return this.statementName;
	}
}
