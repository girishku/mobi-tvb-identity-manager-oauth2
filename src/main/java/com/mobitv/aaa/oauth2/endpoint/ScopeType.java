package com.mobitv.aaa.oauth2.endpoint;

public enum ScopeType {
	AUTHENTICATE_PLAYBACK("authenticate_playback"), REMOTE_CONTROL_SETUP("remote_control_setup");
	
	private final String label;
	
	private ScopeType(final String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
	/**
	 * Performs a reverse lookup on a given label.  It will return the matching enumeration or
	 * <code>null</code> if the label is not recognized.  
	 * 
	 * @param label  label to lookup
	 * @return  matching enumeration or <code>null</code> if the label is not recognized
	 */
	public static ScopeType lookup(final String label) {
		// Linear search
		for (final ScopeType scopeType : ScopeType.values()) {
			if (scopeType.getLabel().equals(label)) {
				return scopeType;
			}
		}
		
		// If this point is reached then no match was found for the given label
		return null;
	}
}
