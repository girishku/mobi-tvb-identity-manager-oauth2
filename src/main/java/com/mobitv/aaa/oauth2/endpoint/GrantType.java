package com.mobitv.aaa.oauth2.endpoint;

public enum GrantType {

	gateway_header,
	password,
	refresh_token,
	authorization_code,
	pin,

	profile_id,

	anonymous_token,
	token_for_token,
	switch_profile,
	associate_profile,
	add_device,
	add_and_register_device,
	access_token;
}
