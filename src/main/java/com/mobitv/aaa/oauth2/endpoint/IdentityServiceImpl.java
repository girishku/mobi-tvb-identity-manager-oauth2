package com.mobitv.aaa.oauth2.endpoint;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.Statement;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.authnz.dto.TokenInfoRequest;
import com.mobitv.aaa.service.AuthorizationCodeService;
import com.mobitv.aaa.service.LoginProfileMappings;
import com.mobitv.aaa.service.OAuth2TokenException;
import com.mobitv.aaa.service.OAuth2TokenService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.aaa.service.TokenFormatException;
import com.mobitv.aaa.service.UserIdentityDTO;
import com.mobitv.aaa.service.impl.AccountManagerHelper;
import com.mobitv.exceptionwrapper.exception.MobiBadRequestException;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.exceptionwrapper.exception.MobiInternalServerErrorException;
import com.mobitv.platform.core.dto.Device;
import com.mobitv.platform.core.dto.Profile;
import com.mobitv.platform.identity.dto.AuthorizationCodeRequest;
import com.mobitv.platform.identity.dto.AuthorizationCodeResponse;
import com.mobitv.platform.identity.dto.Token;
import com.mobitv.platform.identity.dto.TokensRequest;
import com.mobitv.platform.identity.rest.IdentityService;
import com.sun.jersey.spi.resource.Singleton;

@Singleton
@Path("/identity/v5/oauth2")
@Service("identityService")
public class IdentityServiceImpl implements IdentityService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String EMAIL_SUFFIX 	= "@mobitv.com";

	@Autowired
	OAuth2TokenService tokenFactory;
	@Autowired	
	SecureTokenService tokenService;
	@Autowired	
	LoginProfileMappings loginProfileMappings;
	
	@Resource(name = "authorizationCodeService")
	AuthorizationCodeService authorizationCodeService;

	private @Value("${anonymous_scope}") String anonymousScope;
	
	private @Value("${support.gateway.headers}") String supportedHeaders;
	
	private String[] supportedGatewayHeaders;
	
	@Resource
	private AccountManagerHelper accountManagerHelper;
	
	@Context 
	HttpHeaders headers;
	
	/**
	 * For manual injection typically by JUnit
	 * 
	 * @param accountManagerHelper
	 */
	public void setAccountManagerHelper(AccountManagerHelper accountManagerHelper) {
		this.accountManagerHelper = accountManagerHelper;
	}
	
	@PostConstruct
	public void init() {
		if (StringUtils.isNotEmpty(supportedHeaders)) {
			supportedGatewayHeaders = supportedHeaders.split(",");
		} else {
			throw new RuntimeException("atleast 1 gateway header value should be supported as part of the config, default is 'x-mobitv-vuid'");
		}
	}
	
	@SuppressWarnings("rawtypes")
	@POST
	@Path("{carrier}/{product}/{version}/tokens/verify")
    @Produces({ "application/xml", "application/json", "application/x-lua", "text/x-lua" })
	public TokenData verifyAccessToken(
			@PathParam("carrier") String carrier,
			@PathParam("product") String product,
			@PathParam("version") String version,
			TokenInfoRequest tokenInfoRequest) {
		
		try {
			logger.debug("Received verifyAccessToken request for: " + tokenInfoRequest.getAccess_token());
			
			TokenData data = tokenService.unmarshall(tokenInfoRequest.getAccess_token());
			
			//Verify carrier,product version
			if (((data.getCarrier().equals(carrier)) && data.getProduct().equals(product)) == false) {
				logger.warn("carrer, product and version does not match");
				throw new OAuth2TokenException("IDM2002", "invaild_request", "carrer, product and version does not match");
			} 
			
			
			if (!data.getTokenType().equals(OAuth2TokenType.access_token)) {
				logger.warn("Invaild token type : expected access_token");
				throw new OAuth2TokenException("IDM2002", "invaild_request", "Invaild token type : expected access_token");
			}
			
			if (!tokenFactory.isValid(data)) {
				logger.info("refresh_token has expired.");
				throw new OAuth2TokenException("IDM2001", "invaild_request", "this token is expired!");	
			}
			
			return data;
		} catch (MobiException e) {
			// Pass thru mobi exception with their pre-defined HTTP codes for proper error processing
			throw e;
		} catch (Exception e) {
			// Catch all for ISE & logged
			logger.error(e.getMessage(), e);
			throw new MobiException("IDM5001", e.getMessage(), e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Token getTokens(
			String carrier, 
			String product, 
			String version, 
			String xMobitvVuid,
			String alternateVuid, 
			String nai, 
			String wapProfile,
			String userAgent,
			TokensRequest request) {
		
		try {
			String vuid = null;
			String profileId = null;
			String partnerProfileId = null;
			String email = null;
			Profile profile = null;
			Token token = null;
			TokenData tokenData = null;
			
			if (headers != null) {
				System.out.println(headers.getRequestHeaders().toString());
				
				// lookup VUID from the gateway header
				for (String header : supportedGatewayHeaders) {
					List<String> list = headers.getRequestHeader(header);
					if (CollectionUtils.isNotEmpty(list)) {
						vuid = list.get(0);
						break;
					}
				}
			}
			
			logger.debug("Generating token for scope " + request.getScope());
			GrantType grantType = null;
			try{
				grantType = GrantType.valueOf(request.getGrantType());
			}catch(IllegalArgumentException e){
				throw new OAuth2TokenException("IDM2001", "invalid_request", "Invalid grant_type on tokens request.");
			}
			
			switch (grantType) {
				case password:
					String username = request.getUsername();
					String password = request.getPassword();
					
					logger.info("tokens API called, grant_type=password, username={}", username);

					if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
						logger.error("'username' and 'password' can not be null or empty.");
						throw new MobiBadRequestException("IDM1002", 
								"Invalid Request", "'username' and 'password' can not be null or empty.");
					}
					
					String clientId = retrieveOrGenerateClientId(request.getClientId());
					String deviceName = getDeviceName(userAgent);
					logger.info("The device id is : {}", clientId);
					logger.info("The device name is : {}", deviceName);
					
					UserIdentityDTO dto = loginProfileMappings.getProfileForUser(username);
					
					if (dto == null) {
						logger.error("no login available for this user");
						throw new OAuth2TokenException("IDM2002", "invalid_request", "Invalid username on tokens request.");
					}
					
					if(!dto.getPassword().equalsIgnoreCase(password)) {
						logger.error("invalid password for user:{}", username);
						throw new OAuth2TokenException("IDM2002", "invalid_request", "Invalid password on tokens request.");
					}
					
					partnerProfileId = dto.getPartnerProfileId();
					email = dto.getEmail();
					String scope = "role_verified_identity core_services";
					
					profile = this.accountManagerHelper.registerUser(carrier, product, version, partnerProfileId, email);
					
					if (profile == null || StringUtils.isEmpty(profile.getAccountId()) || StringUtils.isEmpty(profile.getProfileId())) {
						throw new MobiInternalServerErrorException("IDM5001", 
								"Mobi Server Error", "Failed to register user");
					}
					
					return tokenFactory.create(carrier, product, profile.getProfileId(), 
							scope, clientId, username, username, wapProfile, userAgent,
							nai, profile.getAccountId());

				case gateway_header:
					logger.info("tokens API called, grant_type=gateway_header, vuid={}", vuid);
					
					if (StringUtils.isEmpty(vuid)) {
						logger.error("VUID header is missing/empty");
						throw new MobiBadRequestException("IDM1003", 
										"Invalid Request", "gateway header for VUID is missing/empty");
					}
									
					// specific to SPRINT carrier only
					if ("sprint".equals(carrier)) {
						if (StringUtils.isEmpty(wapProfile)) {
							logger.error("Header 'x-wap-profile' is missing/empty");
							throw new MobiBadRequestException("IDM1007", 
											"Invalid Request", "Header 'x-wap-profile' is missing/empty");
						}
					}
					
					if (StringUtils.isEmpty(request.getClientId()) || StringUtils.isEmpty(request.getScope())) {
						logger.error("client_id is missing/empty from request body");
						throw new MobiBadRequestException("IDM1008", 
										"Invalid Request", "client_id/scope is missing/empty from the request body");
					}

					try {
						partnerProfileId = Base64.encodeBase64URLSafeString(MessageDigest.getInstance("MD5").digest(vuid.getBytes()));
						email = partnerProfileId + EMAIL_SUFFIX;
					} catch (Exception e) {
						throw new MobiInternalServerErrorException("IDM5001", 
								"Mobi Server Error", e.getMessage());
					}
						
					// setup account (as necessary)
					profile = this.accountManagerHelper.registerUser(carrier, product, version, partnerProfileId, email);
					
					if (profile == null || StringUtils.isEmpty(profile.getAccountId()) || StringUtils.isEmpty(profile.getProfileId())) {
						throw new MobiInternalServerErrorException("IDM5001", 
								"Mobi Server Error", "Failed to register user");
					}
					
					logger.info("account has been created, account_id={} and profile_id={}", profile.getAccountId(), profile.getProfileId());
					return tokenFactory.create(carrier, product, profile.getProfileId(), 
							request.getScope(), request.getClientId(), 
							vuid, alternateVuid, wapProfile, userAgent, nai, 
							profile.getAccountId());
				case anonymous_token:
					profileId = "anonymous-" + UUID.randomUUID().toString();
					return tokenFactory.create(carrier, product, profileId, anonymousScope, false, request.getAccountId());
				case profile_id:			
					if (StringUtils.isEmpty(request.getProfileId()) || StringUtils.isEmpty(request.getScope())) {
						throw new MobiException("IDM1003", "invalid_request", "profile_id or scope is empty", 400);
					}
					return tokenFactory.create(carrier, product, request.getProfileId(), request.getScope(), false, request.getAccountId());
				case refresh_token:
					if (StringUtils.isEmpty(request.getRefreshToken())) {
						throw new MobiException("IDM1004", "invalid_request", "refresh_token is empty", 400);
					}
					token = tokenFactory.renew(carrier, product, request.getRefreshToken());
					tokenData = tokenService.unmarshall(request.getRefreshToken());
					List<Statement> statements = tokenData.getStatements();
					String sessionId = null;
					for (Statement statement : statements) {
						if ("x-mobitv-sid".equals(statement.getName())) {
							sessionId = statement.getValue();
							break;
						}
					}
					List<String> scopes = tokenData.getScope();
					for (String scp : scopes) {
						ScopeType scopeType = ScopeType.lookup(scp);
						if (scopeType == null) {
							continue;
						}

						switch (scopeType) {
							case AUTHENTICATE_PLAYBACK:
								// No need to check device registration, tokens should be given out
								break;
							case REMOTE_CONTROL_SETUP:
								if (!isDeviceRegistered(carrier, product, version, tokenData.getProfileID(), (String) tokenData.getCid(), token.getAccessToken(), sessionId)) {
									throw new MobiException(
											"IDM3005",
											"invalid authorization code",
											"Device not registered to the account of the user attempting to setup remote control mode.",
											401);
								}
								break;
						}
					}
					return token;
				case token_for_token:
					if (StringUtils.isEmpty(request.getAccessToken()) || StringUtils.isEmpty(request.getScope())) {
						throw new MobiException("IDM1005", "invalid_request", "access_token or scope is empty", 400);
					}
					return tokenFactory.exchange(carrier, product, request.getAccessToken(), request.getScope());
				case associate_profile:
					if (StringUtils.isEmpty(request.getProfileId()) || StringUtils.isEmpty(request.getScope())) {
						throw new MobiException("IDM1003", "invalid_request", "profile_id or scope is empty", 400);
					}
					if (StringUtils.isEmpty(request.getAccessToken())) {
						throw new MobiException("IDM1006", "invalid_request", "Invalid data on login request, expected access_token", 400);
					}
					return tokenFactory.associateUser(carrier, product, request.getProfileId(), request.getScope(), request.getAccessToken());
				case authorization_code:
					if (StringUtils.isEmpty(request.getCode())) {
						throw new MobiException("IDM1008", "invalid_request", "code is empty", 400);
					}
					if (StringUtils.isEmpty(request.getClientId())) {
						throw new MobiException("IDM1007", "invalid_request", "client_id is empty", 400);
					}
					
					return generateTokensWithAuthorizationCode(carrier, product, version, request.getCode(), request.getClientId());
				case pin:
					return processPinValidatation(carrier, product, version, request.getRefreshToken(), request.getPin());
				default:
					logger.warn("Invalid grant_type on registration request : expected profile_id");
					throw new OAuth2TokenException("IDM2001", "invalid_request", "Invalid grant_type on tokens request.");
			}
		} catch (MobiException e) {
			// Pass thru mobi exception with their pre-defined HTTP codes for proper error processing
			throw e;
		} catch (Exception e) {
			// Catch all for ISE & logged
			logger.error(e.getMessage(), e);
			throw new MobiException("IDM5001", e.getMessage(), e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Token generateTokensWithAuthorizationCode(String carrier,
			String product, String version, String authorizationCode,
			String clientId) {
		TokenData authorizationCodeTokenData = authorizationCodeService
				.getTokenData(authorizationCode);
		if (authorizationCodeTokenData == null) {
			throw new MobiException("IDM3001", "invalid authorization code",
					"unable to parse authorization code", 500);
		}

		// Ensure auth token is not expired 
		guardAuthCodeExpired(authorizationCodeTokenData, new Date(), "IDM3002", "expired authorization code", "Authorization code has expired. Unable to generate access tokens.", 401);

		if (authorizationCodeTokenData.getCid() == null) {
			throw new MobiException(
					"IDM3003",
					"invalid authorization code",
					"Unable to identify device for which authorization code has been generated.",
					401);
		}

		if (!clientId.equals(authorizationCodeTokenData.getCid())) {
			throw new MobiException(
					"IDM3004",
					"invalid authorization code",
					"Authorization code being used on a device different from what it was generated for.",
					401);
		}

		List<Statement> statements = authorizationCodeTokenData.getStatements();
		String sessionId = null;
		for (Statement statement : statements) {
			if ("x-mobitv-sid".equals(statement.getName())) {
				sessionId = statement.getValue();
				break;
			}
		}

		Token token = tokenFactory.create(authorizationCodeTokenData);

		Device device = accountManagerHelper.devices(carrier, product, version,
				authorizationCodeTokenData.getProfileID(), clientId,
				token.getAccessToken(), sessionId);

		List<String> scopes = authorizationCodeTokenData.getScope();
		for (String scope : scopes) {
			ScopeType scopeType = ScopeType.lookup(scope);
			if (scopeType == null) {
				continue;
			}

			switch (scopeType) {
			case AUTHENTICATE_PLAYBACK:
				// No need to check device registration, tokens should be given out
				break;
			case REMOTE_CONTROL_SETUP:
				if (device == null) {
					throw new MobiException(
							"IDM3005",
							"invalid authorization code",
							"Device not registered to the account of the user attempting to setup remote control mode.",
							401);
				}
				break;
			}
		}

		return token;
	}

	/**
	 * Use this to guard against the token already is expired.  This method will <i>both</i> check for expiration and
	 * throw a {@link MobiException} to block execution with the specified error message.
	 * 
	 * @param authorizationCodeTokenData  token data
	 * @param now                         reference date to check against (almost always is current Date at time of call via new Date())
	 * @param errorCode                   use this error code if throwing MobiException  (ex: "IDM3001")
	 * @param errorMessage                use this error message if throwing MobiException (ex: "expired authorization code")
	 * @param errorDetail                 use this error detail if throwing MobiException (ex: "Authorization code has expired. Unable to generate access tokens.")
	 * @param httpStatus                  use this status code if throwing MobiExceptino (almost always should be 401)
	 */
	@SuppressWarnings("rawtypes")
	private void guardAuthCodeExpired(final TokenData authorizationCodeTokenData, final Date now, String errorCode, String errorMessage, String errorDetail, int httpStatus) {
		Date tokenCreatedDate = authorizationCodeTokenData.getCreationDate();
		int tokenDuration = authorizationCodeTokenData.getExpires_in();
		Date tokenExpiresDate = DateUtils.addSeconds(tokenCreatedDate, tokenDuration);

		if (now.after(tokenExpiresDate)) {
			throw new MobiException(
					errorCode,
					errorMessage,
					errorDetail,
					httpStatus);
		}
	}

	private String retrieveOrGenerateClientId(String clientId) {
		if (clientId == null || clientId.isEmpty()) {
			return UUID.randomUUID().toString();
		}
		return clientId;
	}

	private String getDeviceName(String userAgent) {
		if(userAgent != null && !userAgent.isEmpty()) {
			try {
				String deviceName = URLEncoder.encode(userAgent, "UTF-8");
				return deviceName;
			} catch (UnsupportedEncodingException e) {
				logger.error(e.getMessage());
				throw new MobiInternalServerErrorException("500", "Mobi server error", "Mobi server error");
			}
		}
		return "Mobile";
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public AuthorizationCodeResponse getAuthorizationCode(
			final String carrier,
			final String product,
			final String version, 
			final String x_mobitv_sid,
			final AuthorizationCodeRequest authorizationCodeRequest) {
		// Validate required parameters
		//
		final String responseType = authorizationCodeRequest.getResponseType();
		if (StringUtils.isEmpty(responseType)) {
			throw new MobiBadRequestException("IDM1014", "response_type is required and must be non-null", "response_type is required and must be non-null");
		}
		
		if (!"code".equals(responseType)) {
			throw new MobiBadRequestException("IDM1009", "response_type value is not code", "response_type value is not code");
		}
		
		final String grantType = authorizationCodeRequest.getGrantType();
		if (StringUtils.isEmpty(grantType)) {
			throw new MobiBadRequestException("IDM1015", "grant_type is required and must be non-null", "grant_type is required and must be non-null");
		}
		
		if (!"access_token".equals(grantType)) {
			throw new MobiBadRequestException("IDM1012", "grant_type value is not access_token", "grant_type value is not access_token");
		}
		
		final String scope = authorizationCodeRequest.getScope();
		if (StringUtils.isEmpty(scope)) {
			throw new MobiBadRequestException("IDM1016", "scope is required and must be non-null", "scope is required and must be non-null");
		}
		
		final ScopeType scopeType = ScopeType.lookup(scope);
		if (scopeType == null) {
			String message = String.format("Request 'scope' value '%s' is not recognized.  Recognized values are: %s", scope, StringUtils.join(ScopeType.values(), ", "));
			throw new MobiBadRequestException("IDM1010", message, message);
		}
		
		final String clientId = authorizationCodeRequest.getClientId();
		if (StringUtils.isEmpty(clientId)) {
			throw new MobiBadRequestException("IDM1011", "client_id required and must be non-null", "client_is required and must be non-null");
		}
		
		final String accessToken = authorizationCodeRequest.getAccessToken();
		if (StringUtils.isEmpty(accessToken)) {
			throw new MobiBadRequestException("IDM1013", "access_token is required and must be non-null", "access_token is required and must be non-null");
		}

		TokenData data;
		try {
			data = tokenService.unmarshall(accessToken);
		} catch (TokenFormatException e) {
			// Added for USR-3034
			throw new MobiException("IDM3001", "invalid authorization code", "unable to parse authorization code", 500);
		}
		
		// Ensure auth code is still active
		guardAuthCodeExpired(data, new Date(), "IDM3002", "expired authorization code", "Authorization code has expired. Unable to generate access tokens.", 401);
		
		final String profileId = data.getProfileID();
		final String accountId = data.getAccountID();
		final List<Statement> statements = data.getStatements();
		final List<String> scopes = (data.getScope() != null) ? data.getScope() : new ArrayList<String>();
		
		// At this point, the scope_type will be one of these case statements
		//
		final AuthorizationCodeResponse authorizationCodeResponse = new AuthorizationCodeResponse();
		switch (scopeType) {
			case AUTHENTICATE_PLAYBACK:
				// authenticate_playback - create authorization code
				final String codePlayback = authorizationCodeService.create(carrier, product, version, profileId, accountId, scopes, statements, clientId, null, scopeType);
				
				authorizationCodeResponse.setCode(codePlayback);
				break;
			case REMOTE_CONTROL_SETUP:
				
				// Verify dongle is registered to account
				final Device device = accountManagerHelper.devices(carrier, product, version, profileId, clientId, accessToken, x_mobitv_sid);
				if (device == null) {
					// User is not registered to the given device (clientId)
					throw new MobiException("IDM1006", "User Not Authorized", String.format("Device ID '%s' is not registered to the user", clientId), 401);
				}
				
				// If this point is reached then authorization is granted
				final String codeRemoteControl = authorizationCodeService.create(carrier, product, version, profileId, accountId, scopes, statements, clientId, null, scopeType);
				
				authorizationCodeResponse.setCode(codeRemoteControl);
				break;
		}
		
		// This point is reached only on successful authorization from any of the scope types above
		return authorizationCodeResponse;
	}
	
	private boolean isDeviceRegistered(String carrier, String product, String version, String profileId, String clientId, String accessToken, String x_mobitv_sid) {
		Device device = accountManagerHelper.devices(carrier, product, version, profileId, clientId, accessToken, x_mobitv_sid);
		return (device != null) ? true : false;		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Token processPinValidatation(String carrier, String product, String version, String refreshToken, String pin) {
		if (StringUtils.isEmpty(pin)) {
			throw new MobiException("IDM1009", "invalid_request", "pin is empty", 400);
		}
		if (StringUtils.isEmpty(refreshToken)) {
			throw new MobiException("IDM1004", "invalid_request", "refresh_token is empty", 400);
		}
	
		// generate new tokens, this would also verify refresh_token
		logger.info("breaking open the tokens");
		Token token = tokenFactory.renew(carrier, product, refreshToken);
		TokenData tokenData = tokenService.unmarshall(token.getAccessToken());
		
		Profile newProfile = accountManagerHelper.validatePin(carrier, product, version, tokenData.getProfileID(), pin);
		
		logger.info("generating new tokens with elevated scope");
		String scope = StringUtils.join(tokenData.getScope(), " ") + " pin_verified";
		token = tokenFactory.create(carrier, product, newProfile.getProfileId(), scope, false, newProfile.getAccountId(), tokenData.getStatements(), tokenData.getCid(), true);		
		token.setRefreshToken(null);
		
		return token;
	}
	
}
