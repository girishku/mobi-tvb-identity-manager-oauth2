package com.mobitv.aaa.oauth2.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.authnz.dto.OAuth2TokenType;
import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.authnz.dto.TokenInfoRequest;
import com.mobitv.aaa.oauth2.TokenService;
import com.mobitv.aaa.service.OAuth2TokenException;
import com.mobitv.aaa.service.OAuth2TokenService;
import com.mobitv.aaa.service.SecureTokenService;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.sun.jersey.spi.resource.Singleton;

@Singleton
@Service("tokenService")
public class TokenServiceImpl implements TokenService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TokenServiceImpl.class);

	@Autowired
	private OAuth2TokenService tokenFactory;

	@Autowired
	private SecureTokenService tokenService;

	@SuppressWarnings("rawtypes")
	@Override
	public TokenData verify(String carrier, String product,
			String version, TokenInfoRequest tokenInfoRequest) {
		try {
			LOGGER.debug("Received verifyAccessToken request for: {}",
					tokenInfoRequest.getAccess_token());

			TokenData data = tokenService.unmarshall(tokenInfoRequest
					.getAccess_token());

			// Verify carrier,product version
			if (!((data.getCarrier().equals(carrier)) && data.getProduct()
					.equals(product))) {
				LOGGER.warn("{}", "carrier and product do not match");
				throw new OAuth2TokenException("IDM2002", "invalid_request",
						"carrier and product does not match");
			}

			if (!data.getTokenType().equals(OAuth2TokenType.access_token)) {
				LOGGER.warn("{}", "Invalid token type : expected access_token");
				throw new OAuth2TokenException("IDM2002", "invalid_request",
						"Invalid token type : expected access_token");
			}

			if (!tokenFactory.isValid(data)) {
				LOGGER.info("{}", "refresh_token has expired");
				throw new OAuth2TokenException("IDM2001", "invalid_request",
						"this token is expired");
			}

			return data;
		} catch (MobiException e) {
			// Pass thru mobi exception with their pre-defined HTTP codes for
			// proper error processing
			throw e;
		} catch (Exception e) {
			// Catch all for ISE & logged
			LOGGER.error(e.getMessage(), e);
			throw new MobiException("IDM5001", e.getMessage(), e.getMessage(),
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

}
