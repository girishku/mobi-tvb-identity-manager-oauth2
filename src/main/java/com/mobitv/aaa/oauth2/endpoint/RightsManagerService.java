package com.mobitv.aaa.oauth2.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.purchasemanager.dto.rights.PurchaseRightsRequestData;
import com.mobitv.aaa.purchasemanager.dto.rights.PurchaseRightsResponseData;
import com.mobitv.aaa.service.AssetPropertyHolder;
import com.mobitv.aaa.service.AssetPropertyHolderList;
import com.mobitv.aaa.to.AgreementTO;
import com.mobitv.aaa.to.AssetTO;
import com.mobitv.aaa.to.ContextTO;
import com.mobitv.aaa.to.ExecuteTO;
import com.mobitv.aaa.to.GlobalContextTO;
import com.mobitv.aaa.to.PermissionTO;
import com.mobitv.aaa.to.RequirementTO;
import com.mobitv.aaa.to.RightsTypeTO;
import com.mobitv.exceptionwrapper.exception.MobiBadRequestException;
import com.sun.jersey.spi.resource.Singleton;

@Singleton
@Path("/core/v5/rights")
@Service("rightsService")
public class RightsManagerService {

	
	@Autowired
	AssetPropertyHolderList assetProperties;
	
	@GET
	@Path("health")
	@Produces({"text/plain"})
	public Response health() {
		return Response.status(Status.OK).entity("OK").build();
	}
	
	@GET
	@Path("{carrier}/{product}/{version}/{profileId}/{sku}")
	@Produces({"application/xml", "application/json", "application/x-lua", "text/x-lua"})
	public Response rights(@PathParam("profileId") String profileId, 
			@PathParam("sku") String sku){
		
		RightsTypeTO rightsTO = new RightsTypeTO();
		
		// Set global context
		GlobalContextTO globalContextTO = new GlobalContextTO();
		globalContextTO.setProfile(profileId);		
		// Add global context to the rights object
		rightsTO.setGlobalContextTO(globalContextTO);

		
		AgreementTO agreement = new AgreementTO();

		// Set agreement context
		ContextTO context = new ContextTO();

		context.setUid(sku);
		context.setVersion("1.0.0");

		agreement.setContext(context);
		
		
		// We have multiple assetProperties per sku, and each has different roles of access
		for (AssetPropertyHolder assetProperty : assetProperties.getAssets()) 
		{    
			AssetTO asset = new AssetTO();
			asset.setId(assetProperty.getName());
			
			agreement.addAsset(asset);
			
			PermissionTO permission = new PermissionTO();
			ExecuteTO execute = new ExecuteTO();
			
			if (assetProperty.getAccepts() != null)  {
				RequirementTO requirement = new RequirementTO();
				requirement.setAccept(assetProperty.getAccepts());
				// Add requirement to execute
				execute.setRequirement(requirement);
			}
			
			// Add a reference to the asset to execute
			AssetTO refAsset = new AssetTO();
			refAsset.setIdref(asset.getId());
			execute.setAsset(refAsset);
			
			permission.setExecute(execute);
			
			agreement.addPermission(permission);
		}	
		rightsTO.addAgreement(agreement);
		
		CacheControl cacheControl = new CacheControl();
		cacheControl.setMaxAge(10000);
		
		return Response.ok(rightsTO).cacheControl(cacheControl).build();
	}

	@POST
	@Path("{carrier}/{product}/{version}/{profile_id}/purchase_rights")
	@Produces({"application/xml", "application/json", "application/x-lua", "text/x-lua"})
	@Consumes({"application/xml", "application/json", "application/x-lua", "text/x-lua"})
	public Response purchaseRights(
			@PathParam("carrier") String carrier,
			@PathParam("product") String product,
			@PathParam("version") String version,
			@PathParam("profile_id") String profileId,
			@QueryParam("action") String action,
			PurchaseRightsRequestData request) throws Exception {
		
		if (request.getPurchaseId() == null) {
			throw new MobiBadRequestException("RM4501", "Invalid request", "purchase_id is empty");
		}
		
		if (StringUtils.isEmpty(request.getAccountId())) {
			throw new MobiBadRequestException("RM4502", "Invalid request", "account_id is empty");
		}
		
		String timeInMillis = System.currentTimeMillis() + "";
		String purchaseRightsId = StringUtils.substring(timeInMillis, timeInMillis.length()/2);
		
		PurchaseRightsResponseData response = new PurchaseRightsResponseData();
		response.setPurchaseRightsId(Long.parseLong(purchaseRightsId));
		
		return Response.status(Status.OK).entity(response).build();
	}

}
