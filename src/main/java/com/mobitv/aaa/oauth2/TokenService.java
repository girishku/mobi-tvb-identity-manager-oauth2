package com.mobitv.aaa.oauth2;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.mobitv.aaa.authnz.dto.TokenData;
import com.mobitv.aaa.authnz.dto.TokenInfoRequest;

@Path("/platform/v5/oauth2")
public interface TokenService {

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/{carrier}/{product}/{version}/tokens/verify")
    @Produces({ "application/xml", "application/json", "application/x-lua", "text/x-lua" })
	@Consumes({ "application/xml", "application/json", "application/x-lua", "text/x-lua" })
	TokenData verify(@PathParam("carrier") String carrier,
			@PathParam("product") String product,
			@PathParam("version") String version,
			TokenInfoRequest tokenInfoRequest);

}
